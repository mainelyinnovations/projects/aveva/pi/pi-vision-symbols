/**
 * This file contains helpful information about how to possibly handle different languages.
 */

// Init
const userLang = navigator.language || navigator.userLanguage;
let i;

if (userLang == "pt" || userLang == "pt-BR") {
  for (i = 0; i < dataValues.Values.length; i++) {
      dataValues.Values[i].Value = dataValues.Values[i].Value.replace(".","").replace(",",".");
  }
}

// Insertupdateordelete
let data;

if (userLang == "pt" || userLang == "pt-BR") {
  const dateTimeSplit = datastream.Time.split(" ");
  const dateSplit = dateTimeSplit[0].split("/");
  const timeSplit = dateTimeSplit[1].split(":");
  const dateTimeString = new Date(dateSplit[2],dateSplit[1]-1,dateSplit[0],timeSplit[0],timeSplit[1],timeSplit[2])

  data = {
    "Timestamp": dateTimeString.toISOString(),
    "Value": datastream.Value.replace(",",".")
  };
}
else {
  data = {
    "Timestamp": datastream.Time,
    "Value": datastream.Value
  };
}
