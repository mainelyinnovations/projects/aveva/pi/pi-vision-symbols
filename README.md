# PI Vision Custom Symbols

This repository contains tools for developing custom PI Vision symbols using the extensibility framework and TypeScript, along with a collection of open-source symbols. Review the [Development](#symbol-development) section for information on how to make use of the TypeScript library.

_Note_: These types are applicable to `PI Vision 2023 SP1 Patch 1`.

## Usage

This library supports building PI Vision symbols with TypeScript, enhancing the development experience. Each symbol resides in its own directory under `src/Symbol`. The HTML and CSS file development remains consistent with the AVEVA PI Vision Extensibility Guide. The main difference lies in using TypeScript `.ts` files instead of JavaScript `.js` files. Types are provided at `src/Type` to facilitate easier symbol development. The `Button Link` symbol serves as a reference for utilizing these types.

To obtain the necessary JavaScript files for deployment/installation, install the required tools using either `npm` or `yarn`.

```bash
npm install
```

```bash
yarn install
```

This will install the development tools necessary for TypeScript. Once the tools are installed and a symbol is ready for deployment, generate the production-built files using the following command:

```bash
npm run prepare
```

## Available Symbols

The following items are the list of symbols currently available.

- Data
  - [Timeseries Value](./src/Symbol/Data/TimeseriesValue/Readme.md)
- External
  - [Nested Vision Display](./src/Symbol/External/NestedVisionDisplay/Readme.md)
- Layout
  - [Button Link](./src/Symbol/Layout/ButtonLink/Readme.md)
- Visual
  - [SVG](./src/Symbol/Visual/SVG/Readme.md)

## Common Utilities

Common utilities used within the custom symbols have been built to reduce coding effort. Ensure all `src/Symbol/Common` files are included in the deployment of symbols from this repository to the target PI Vision servers. If you are not deploying symbols from this library and do not use the features exposed from these classes, then you do not need to import them with your deployments.

## Installing a Symbol

To deploy a symbol in an environment, follow these steps:

- Run `npm run prepare` to convert TypeScript files to JavaScript and obtain the output `lib` folder. This folder is where the files will be copied from for deployment to a PI Vision server.
- Copy the contents of the symbol's folder (after producing the built files, excluding the files listed below) to the designated PI Vision server.
  - Exclude:
    - The contents of any images directory (used for Readme documents).
    - Readme.md
- Place the contents of the symbol files in `%PIHOME64%\PIVision\Scripts\app\editor\symbols\ext`.
  - Files from an `icons` folder must go in the icons folder in this directory. All other files go into the directory specified above.
  - Copy the contents of [external libraries](./src/Libraries/) into the `libraries` folder in this directory (including the folder structure).
    - The one exception is that the `webfonts` folder of the `Font Awesome` library must be uploaded to the `PIVision/Content/webfonts` folder instead.
  - Copy the contents of [common classes](./src/Symbol/Common/) into this directory.
- Restart the PI Vision Service pool.
  - _Note_: This is only required when first installing a symbol, not when updating a symbol.

## Code Snippets

When using [Visual Studio Code](https://code.visualstudio.com/), code snippets can be used to scaffold a file or section of code. The available code snippets for this project are included in the `.vscode` folder.

- pisymcode.code-snippets
  - This snippet allows typing `aveva:pisymcode` to bootstrap a basic TypeScript version of a custom PI Vision Symbol.
- pisymdoc.code-snippets
  - This snippet allows typing `aveva:pisymdoc` to bootstrap a basic custom PI Vision Symbol Readme document.
- pisymtype.code-snippets
  - This snippet allows typing `aveva:pisymtype` to bootstrap a basic TypeScript declaration file for the custom PI Vision Symbol to identify the symbol's parameters.

## Symbol Development

The use of TypeScript in building PI Vision symbols requires slight modifications to the AVEVA documentation for building a symbol. For the most part, these modifications are syntactical and for development only (none of these changes are actually output in the production built files).

To start, each symbol requires a `declarations.d.ts` file. This file defines the configuration (types) of the symbol and the available methods & properties on the `scope` of the symbol and will look like this:

```ts
export interface VisionSymbolConfig extends BaseSymbolConfig {
  Height: number;
  Width: number;
  BackgroundColor: string;
  BorderColor: string;
  TableBodyBackgroundColor: string;
  TableBodyBorderColor: string;
  FontSize: number;
  LabelColor: string;
}

export interface VisionScope extends BaseVisionScope<VisionSymbolConfig> {
  ParentPath: string;
  LastUpdate: Date;
  Values: object;
  getSymbolStyles: () => object;
  getFontSize: () => unknown;
  getTableRowStyles: () => unknown;
  getAlertStyles: (alertType: string) => unknown;
  getLabelStyles: () => unknown;
  getButtonStyles: (alertType: string) => unknown;
}
```

By defining the types for a given symbol, we can then import the types (without using module syntax) by adding the following lines of code at the top of the symbol declaration.

```ts
(function (PV: vision.Visualization) {
  type VisionSymbolConfig = import("./declarations").VisionSymbolConfig;
  type VisionScope = import("./declarations").VisionScope;

  // symbol config & implementation
});
```

If we are injecting services into our symbol, then we need to define an array of the services we want (these will be type checked using TypeScript and only valid entries will be allowed).

```ts
const injectedServices = ["webServices", "$http"] as const satisfies ReadonlyArray<keyof vision.SymbolDefinitionInjectMap>;

type InjectedServiceType = (typeof injectedServices)[number];
```

With our injected services defined, we can create our `symbolVis` function using our types.

```ts
const symbolVis = function () {} as unknown as vision.SymbolVis<VisionScope, InjectedServiceType>;

// If we're not injecting anything, then we can omit the type.
const symbolVis = function () {} as unknown as vision.SymbolVis<VisionScope>;
```

We can then build our symbol definition with our types and our injected services (if we have any). In this example, we use `PV.Extensibility.Enums.DataShapes` which is defined in a custom [PV Extension](./src/Symbol/Common/sym-mi-com-pv-extend.ts) file.

```ts
const definition: vision.SymbolDefinition<VisionSymbolConfig, VisionScope, InjectedServiceType> = {
  // other config
  getDefaultConfig: function () {
    return {
      DataShape: PV.Extensibility.Enums.DataShapes.Table,
    };
  },
  inject: injectedServices,
};

// If we're not injecting anything, then the definition looks like this:
const definition: vision.SymbolDefinition<VisionSymbolConfig, VisionScope> = {
  // other config
  getDefaultConfig: function () {
    return {
      DataShape: PV.Extensibility.Enums.DataShapes.Table,
    };
  },
};
```

With our symbol defined, we can then implement the `init`, `onDataUpdate`, and other optional methods and have proper typing with little effort.

```ts
// We group the injected services into a single array so we can obtain the correct types for each service.
symbolVis.prototype.init = function (
  this: vision.SymbolVis<VisionScope, InjectedServiceType>,
  scope: VisionScope,
  elem: JQuery,
  ...args: vision.SymbolDefinitionArgs<InjectedServiceType>
): void {
  // Injected parameters
  const [webServices, displayProvider, assetContext] = PV.MainelyInnovations.getInjectedParams(injectedServices, args);

  // #region Base Symbol Methods
  this.onDataUpdate<vision.DataShapeValue> = function (newData) {
    if (!newData) {
      return;
    }
  };

  this.onConfigChange<VisionSymbolConfig> = function (newConfig?, oldConfig?) {
    // Ensure both configuration properties are set and not equal to each other.
    if (!newConfig || !oldConfig || angular.equals(newConfig, oldConfig)) {
      return;
    }

    // Handle configuration changes
  };
  // #endregion
};

// If no services are injected, then the `init` function can look like this:
symbolVis.prototype.init = function (this: vision.SymbolVis<VisionSymbolConfig>, scope: VisionScope, elem: JQuery): void {
  // #region Base Symbol Methods
  this.onDataUpdate<vision.DataShapeValue> = function (newData) {
    if (!newData) {
      return;
    }
  };

  this.onConfigChange<VisionSymbolConfig> = function (newConfig?, oldConfig?) {
    // Ensure both configuration properties are set and not equal to each other.
    if (!newConfig || !oldConfig || angular.equals(newConfig, oldConfig)) {
      return;
    }

    // Handle configuration changes
  };
  // #endregion
};
```

With the code above, `PV.MainelyInnovations.getInjectedParams` is a custom function from the [Mainely Innovations extension](./src/Symbol/Common/sym-mi-com-utility.ts) file. This method takes in the `injectedServices` we defined and the grouped `args` from the init function and returns an array with each service with the correct type for the service. When we destructure the results using `[webServices, displayProvider, ...]` we will automatically we warned if we are destructuring too many variables from the array, based on the length of the `injectedServices` array.

To ensure proper typing of the base PI Vision symbol methods, the first parameter specified in the `init` function is defined as `this: SymbolVis`. Doing this allows us to use the types built as part of this framework to ensure we are building symbols correctly.

`this.onDataUpdate` requires specifying the data shape, which is based on the `PV.Extensibility.Enums.DataShapes` enum passed into the symbol configuration. This will provide the correct data type for each of the available symbol data types (value, gauge, etc).

`this.onConfigChange` requires passing in the symbol's unique `VisionSymbolConfig` defined in its `declaration.d.ts` file, so we are able to properly access the symbol's configuration properties.

## Notes

Keep in mind that some symbols may require specific permissions or roles to run. Installing symbols with errors can cause unexpected behavior with PI Vision, so it's recommended to test them in a non-production environment before deploying to production.

## Contribute

If you have anything to contribute or find any bugs, please open a pull request or issue on the repository. Contributions improving the functionality of this extensibility library are welcome.

## Additional Resources

- Maintained by [Mainely Innovations](https://mainely.io)
- [PI Vision Extensibility Guide](https://github.com/osisoft/OSI-Samples-PI-System/blob/main/docs/PI-Vision-Extensibility-Docs/PI%20Vision%20Extensibility%20Guide.md)
- [TypeScript documentation](https://www.typescriptlang.org/docs/)

Please note that the tools and symbols in this repository are provided as-is and is not officially supported by AVEVA. Use the library at your own risk and test thoroughly in a non-production environment before using in production.
