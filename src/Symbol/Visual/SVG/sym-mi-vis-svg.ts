(function (PV: vision.Visualization) {
  ("use strict");

  type VisionSymbolConfig = import("./declarations").VisionSymbolConfig;
  type VisionScope = import("./declarations").VisionScope;
  type AvailableVectors = import("./declarations").AvailableVectors;

  // #region Symbol injected parameters
  // Define the services to inject.
  const injectedServices = ["$sce", "$http", "mainelyInnovations"] as const satisfies ReadonlyArray<keyof vision.SymbolDefinitionInjectMap>;

  // Create a union type from the array
  type InjectedServiceType = (typeof injectedServices)[number];
  // #endregion

  // Base extensibility setup
  const symbolVis = function () {} as unknown as vision.SymbolVis<VisionScope, InjectedServiceType>;
  PV.deriveVisualizationFromBase(symbolVis);

  // #region Constants
  const SYMBOL_NAME = "mi-vis-svg";
  const SYMBOL_DISPLAY_NAME = "SVG";
  const SYMBOL_DEFAULT_CONFIG_KEYS = ["VectorRotation", "VectorColor", "ShowLabel", "LabelAlignment", "LabelColor", "TextSize", "SelectedVector"];
  const AVAILABLE_VECTORS: AvailableVectors = [
    { Name: "Battery" },
    { Name: "Diode" },
    { Name: "Ground" },
    { Name: "Motor" },
    { Name: "Switch" },
    { Name: "Transformer" },
    { Name: "Valve" },
  ];
  // #endregion

  // #region Symbol Definition
  const definition: vision.SymbolDefinition<VisionSymbolConfig, VisionScope, InjectedServiceType> = {
    typeName: SYMBOL_NAME,
    displayName: SYMBOL_DISPLAY_NAME,
    visObjectType: symbolVis,
    datasourceBehavior: PV.Extensibility.Enums.DatasourceBehaviors.Single,
    iconUrl: `${PV.MainelyInnovations.symbolIconPath}/sym-${SYMBOL_NAME}.svg`,
    supportsCollections: true,
    configTemplateUrl: `${PV.MainelyInnovations.symbolBasePath}/sym-${SYMBOL_NAME}-config.html`,
    configTitle: `${PV.ResourceStrings.FormatKeyword} ${SYMBOL_DISPLAY_NAME}`,
    selectSymbolDefaultsFromConfig: PV.MainelyInnovations.selectSymbolDefaultsFromConfig(SYMBOL_DEFAULT_CONFIG_KEYS),
    configInit: PV.SymMIVisSVG.init({ AvailableVectors: AVAILABLE_VECTORS }),
    getDefaultConfig: function () {
      return {
        DataShape: PV.Extensibility.Enums.DataShapes.Value,
        Height: 50,
        Width: 50,
        VectorRotation: 0,
        VectorColor: "rgba(178, 199, 221, 0.95)",
        ShowLabel: true,
        LabelAlignment: "center",
        LabelColor: "rgba(255, 255, 255, 1)",
        TextSize: 12,
        SelectedVector: AVAILABLE_VECTORS[0].Name,
      };
    },
    StateVariables: ["VectorColor"],
    StateProperties: {
      VectorColor: PV.ResourceStrings.ShapeFillKeyword,
    },
    inject: injectedServices,
  };
  // #endregion

  /**
   * Symbol Initialization
   *
   * @param scope - Reference to Vision extensible symbol definition and instance of this symbol.
   * @param elem - jQuery array containing the HTML Element of the symbol.
   * @param args - Array of injected services as configured in the service definition.
   */
  symbolVis.prototype.init = function (
    this: vision.SymbolVis<VisionScope, InjectedServiceType>,
    scope: VisionScope,
    elem: JQuery,
    ...args: vision.SymbolDefinitionArgs<InjectedServiceType>
  ): void {
    // Injected parameters
    const [$sce, $http, mainelyInnovations] = PV.MainelyInnovations.getInjectedParams(injectedServices, args);

    // #region Scope Variables
    scope.VectorContentRaw = "";
    scope.VectorContent = "";
    scope.SymbolVectorElement;
    scope.MultiStateOff = true;
    scope.Label = "...";
    scope.LastColors;
    scope.VectorColor;
    // #endregion

    // #region HTML Elements
    const symbolRootElement = elem.find(`.${SYMBOL_NAME}`);
    // #endregion

    // #region Base Symbol Methods
    (function activate() {
      // Retrieve the SVG content to handle initial rendering of the symbol.
      getVector(scope.config.SelectedVector);
    })();

    /**
     * Process symbol data update events.
     *
     * @param newData - Object with properties determined by the symbol DataShape.
     */
    this.onDataUpdate<vision.DataShapeValue> = function (newData) {
      // Handle turning multi-state off to set the color of the SVG back to the
      // configured value.
      if (!scope.VectorColor && !scope.MultiStateOff) {
        setVectorElement(scope.config.VectorColor);
        scope.MultiStateOff = true;
      }
    };

    /**
     * Process changing configuration of the symbol.
     *
     * @param newConfig - Object with the new configuration properties.
     * @param oldConfig - Object with the old configuration properties.
     */
    this.onConfigChange<VisionSymbolConfig> = function (newConfig, oldConfig) {
      // Ensure both configuration properties are set and not equal to each other.
      if (!newConfig || !oldConfig || angular.equals(newConfig, oldConfig)) {
        return;
      }

      // Change the SVG content if the user chose a new vector.
      if (newConfig.SelectedVector !== oldConfig.SelectedVector) {
        getVector(newConfig.SelectedVector);
      }

      // Change the color of the SVG if the user chose a new color.
      if (newConfig.VectorColor !== oldConfig.VectorColor) {
        setVectorElement(newConfig.VectorColor);
      }
    };

    /**
     * Process changes on the symbol when the instance's multi-state
     * configuration changes. This includes editing the multi-state parameters
     * and when the symbol receives value updates that could result it a change to the
     * currently active multi-state.
     */
    this.onMultistateUpdate = function () {
      const colors = colorProperties(scope);

      // Only change the SVG color if the MS property `scope.VectorColor` is set
      // and there is a color difference between the current update and the previous
      // update.
      //
      // `scope.VectorColor` will be set to undefined when the symbol is first initialized
      // and `colors` and `scope.lastColors` will be different, causing an unintended change.
      if (scope.VectorColor && !angular.equals(colors, scope.LastColors)) {
        // Set the flag that multi-state is currently running.
        if (scope.MultiStateOff) {
          scope.MultiStateOff = false;
        }

        scope.LastColors = colors;
        setVectorElement(scope.VectorColor);
      }
    };

    /**
     * Process events where the symbol is resized.
     *
     * @param width - New width of the symbol.
     * @param height - New height of the symbol.
     */
    this.onResize = function (width, height) {
      if (scope.SymbolVectorElement) {
        // Setting these properties must be done on the `svg` HTML element to properly
        // perform a resize. The SVG (.svg) file must be defined without these properties.
        scope.SymbolVectorElement.setAttribute("width", `${width}`);
        scope.SymbolVectorElement.setAttribute("height", `${height}`);
      }
    };
    // #endregion

    // #region Styling Methods
    /**
     * Get the Main Symbol Styles
     *
     * This method builds the styles for the symbol based on its configuration.
     *
     * @returns Style properties for the symbol.
     */
    scope.getSymbolMainStyles = function () {
      return {
        transform: `rotate(${scope.config.VectorRotation}deg)`,
      };
    };

    /**
     * Get the Symbol Label Styles
     *
     * This method builds the styles for the symbol's label based on its configuration.
     *
     * @returns Style properties for the label.
     */
    scope.getSymbolLabelStyles = function () {
      return {
        color: scope.config.LabelColor,
        "text-align": scope.config.LabelAlignment,
        "font-size": scope.config.TextSize + "px",
      };
    };
    // #endregion

    // #region Symbol Service Setup
    mainelyInnovations.updateSymbolSingleLabel(this);
    // Setup available vectors for configuration pane.
    scope.runtimeData.AvailableVectors = AVAILABLE_VECTORS;
    // #endregion

    // #region Symbol Helper Methods
    /**
     * Retrieves the color properties to apply to the symbol based on its current multi-state status.
     *
     * @param scope - Reference to Vision extensible symbol definition and instance of the provided symbol.
     * @returns Object with the color properties for the symbol.
     */
    function colorProperties(scope: VisionScope) {
      return {
        VectorColor: PV.MainelyInnovations.getSafeColor(scope.VectorColor, scope.config.VectorColor),
      };
    }

    /**
     * Retrieves the SVG content of a vector from the Vision server based on the provided vector name.
     * The retrieved SVG is then updated with the appropriate color based on the symbol's current state
     * before setting the HTML content of the symbol.
     *
     * @param vectorName - Name of the vector to retrieve from the server.
     */
    async function getVector(vectorName: string) {
      // Find the available vector based on the provided name.
      const vector = AVAILABLE_VECTORS.find((i) => i.Name === vectorName);

      try {
        const response = await $http.get(
          `${PV.MainelyInnovations.symbolIconPath}/${SYMBOL_NAME}/${
            // Use the provided vector FileName or Name if possible, otherwise
            // use the first item in the available vector array.
            // File names are only applicable if `Name` contains special characters.
            vector.FileName || vector.Name || AVAILABLE_VECTORS[0].FileName || AVAILABLE_VECTORS[0].Name
          }.svg`,
        );

        // Update the `raw` content variable of the vector with the SVG contents
        // so that we can reuse the original SVG contents if the desired color for
        // the vector is changed.
        scope.VectorContentRaw = response.data as string;
        setVectorElement(scope.VectorColor || scope.config.VectorColor);
      } catch (error) {
        console.error("Error loading SVG:", error);
      }
    }

    /**
     * Sets the content of the SVG element with a new color. This function triggers a change in color
     * when SVGs are first loaded, when the current multi-state status changes, or when multi-state is off
     * and the user changes the color via configuration.
     *
     * @param color - Color to fill the SVG with.
     */
    function setVectorElement(color: string) {
      // Replace the SVG `fill` property content with the current expected color (MS or user-defined).
      const content = scope.VectorContentRaw.replace(/fill="{{scope.VectorColor}}"/g, 'fill="' + color + '"');
      // Set the content to trusted HTML.
      scope.VectorContent = $sce.trustAsHtml(content);

      // Get a reference to the SVG element since it's bound to an HTML load.
      if (!scope.SymbolVectorElement) {
        scope.SymbolVectorElement = symbolRootElement.find("div").find("svg")[0] || undefined;
      }
    }
    // #endregion
  };

  PV.symbolCatalog.register(definition);
})((window as any).PIVisualization);
