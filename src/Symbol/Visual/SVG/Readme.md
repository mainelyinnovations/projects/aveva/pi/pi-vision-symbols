# SVG

The SVG symbol enables the display of SVG content while supporting multi-state functionality. The contents of the SVG vector dynamically change based on the current multi-state configuration. Additionally, when multi-state is disabled, users have the flexibility to choose the color of the SVG content.

## Symbol Information

- Supports Collections: `true`
- Supports Default Configuration: `true`
- DataShape: `Value`
  - This symbol is a single data source with a single value at a specific time.
- DatasourceBehavior: `Single`
  - The symbol is designed to work with a single data source, either a PI Point or an AF Attribute.

## Config

### Core Options

- Vector
  - Defines the SVG content (graphic) for the symbol.
- Rotation
  - Defines the rotation angle of the SVG content.

### Style

- Vector Color
  - Defines the color of the SVG vector.
- Text Size
  - Sets the size for all fonts used within the symbol.
- Label Color
  - Defines the color of the label text.

### Visibility

- Show Label
  - Determines if a label will be shown for the symbol.
- Label
  - Defines the PI Point, portion of the AF attribute path, or custom text to display for the label.
- Label Alignment
  - Defines the text alignment for the label.
