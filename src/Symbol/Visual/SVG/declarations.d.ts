export interface VisionSymbolConfig extends vision.BaseSymbolConfig {
  Height: number;
  Width: number;
  VectorRotation: number;
  VectorColor: string;
  SelectedVector: string;
  ShowLabel: boolean;
  LabelAlignment: string;
  LabelColor: string;
  TextSize: number;
  Multistates?: Array<vision.SymbolMultiState>;
}

export interface RunTimeData extends vision.BaseSymbolRuntimeData<VisionSymbolConfig> {
  AvailableVectors: AvailableVectors;
}

export interface VisionScope extends vision.BaseVisionScope<VisionSymbolConfig, RunTimeData> {
  VectorColor?: string;
  VectorContentRaw: string;
  VectorContent: string;
  SymbolVectorElement: SVGSVGElement | undefined;
  MultiStateOff: boolean;
  Label: string;
  LastColors: {
    VectorColor: string;
  };
  getSymbolMainStyles: () => { [key: string]: string };
  getSymbolLabelStyles: () => { [key: string]: string };
}

export interface SymbolConfigInit {
  init: (options: SymbolConfigInitOptions) => (scope: VisionScope) => void;
}

export interface SymbolConfigInitOptions {
  AvailableVectors: AvailableVectors;
}

export type AvailableVectors = Array<{ Name: string; FileName?: string }>;
