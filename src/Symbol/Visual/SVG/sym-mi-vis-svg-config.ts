(function (PV: vision.Visualization) {
  "use strict";

  // Types
  type VisionScope = import("./declarations").VisionScope;
  type SymbolConfigInitOptions = import("./declarations").SymbolConfigInitOptions;

  PV.SymMIVisSVG = (function () {
    /**
     * Perform configuration initialization for the SVG symbol.
     *
     * - Initializing label settings.
     * - Adding runtime data.
     *
     * @param options - Options for configuration initialization.
     */
    function init(options?: SymbolConfigInitOptions) {
      /**
       * Function passed to `configInit` in symbol definitions.
       *
       * @param scope Reference to Vision extensible symbol definition and instance of the provided symbol.
       */
      return function (scope: VisionScope): void {
        const runtimeData = scope.runtimeData;

        // Configure label settings.
        PV.MainelyInnovations.configInit({ labelSettingsInit: true })(scope);

        // Set the available vectors for usage with the configuration pane.
        runtimeData.AvailableVectors = options.AvailableVectors;
      };
    }

    return {
      init,
    };
  })();
})(window.PIVisualization);
