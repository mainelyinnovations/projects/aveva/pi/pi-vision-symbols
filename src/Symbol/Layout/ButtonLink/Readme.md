# Button Link

The Button Link symbol presents a visual representation of a link in the form of a button, allowing end-users to interact with external resources.
The data source may either be a PI Point or an AF Attribute, but the value must be a string type.

The symbol supports styling options to fit the button into existing themes and layouts.

## Requirements

- Bootstrap CSS

## Symbol Definition

- Supports Collections: `true`
- Supports Default Configuration: `true`
- DataShape: `Value`
  - This symbol utilizes a single value for a given datasource at a specific point in time.
- Datasource Behavior: `Single`
  - The symbol is designed to work with a single data source, either a PI Point or an AF Attribute.

## Symbol Config

### Core Options

- Text
  - Specifies the text displayed on the screen for the link.
- Open New Tab
  - Determines whether the link will open in the same tab or a new tab.

### Styling

- Text Size
  - Sets the size for all fonts used within the symbol.
- Text Color
  - Specifies the text color on the button.
- Button Color
  - Defines the background color of the button.
- Border Color
  - Determines the color used for the border of the button.
- Border Size
  - Sets the border size (width) for the button.
