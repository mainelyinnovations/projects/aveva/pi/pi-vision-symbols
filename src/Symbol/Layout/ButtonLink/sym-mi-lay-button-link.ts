(function (PV: vision.Visualization) {
  ("use strict");

  // Import types
  type VisionSymbolConfig = import("./declarations").VisionSymbolConfig;
  type VisionScope = import("./declarations").VisionScope;

  // Base extensibility setup
  const symbolVis = function () {} as unknown as vision.SymbolVis<VisionScope>;
  PV.deriveVisualizationFromBase(symbolVis);

  // #region Constants
  const SYMBOL_NAME = "mi-lay-button-link";
  const SYMBOL_DISPLAY_NAME = "Button Link";
  const SYMBOL_DEFAULT_CONFIG_KEYS = ["OpenNewTab", "ButtonText", "ButtonColor", "BorderColor", "BorderSize", "TextSize", "TextColor"];
  const TARGET_SELF = "_self";
  const TARGET_BLANK = "_blank";
  // #endregion

  // #region Symbol Definition
  const definition: vision.SymbolDefinition<VisionSymbolConfig, VisionScope> = {
    typeName: SYMBOL_NAME,
    displayName: SYMBOL_DISPLAY_NAME,
    visObjectType: symbolVis,
    datasourceBehavior: PV.Extensibility.Enums.DatasourceBehaviors.Single,
    iconUrl: `${PV.MainelyInnovations.symbolIconPath}/sym-${SYMBOL_NAME}.svg`,
    supportsCollections: true,
    configTemplateUrl: `${PV.MainelyInnovations.symbolBasePath}/sym-${SYMBOL_NAME}-config.html`,
    configTitle: `${PV.ResourceStrings.FormatKeyword} ${SYMBOL_DISPLAY_NAME}`,
    selectSymbolDefaultsFromConfig: PV.MainelyInnovations.selectSymbolDefaultsFromConfig(SYMBOL_DEFAULT_CONFIG_KEYS),
    configInit: PV.MainelyInnovations.configInit({ labelSettingsInit: false }),
    getDefaultConfig: function () {
      return {
        DataShape: PV.Extensibility.Enums.DataShapes.Value,
        Height: 40,
        Width: 100,
        OpenNewTab: true,
        ButtonText: "Link Text",
        ButtonColor: "rgba(67, 105, 131, 0.51)",
        BorderColor: "rgba(67, 105, 131, 0.51)",
        BorderSize: 1,
        TextSize: 14,
        TextColor: "rgba(245, 245, 245, 0.97)",
      };
    },
  };
  // #endregion

  /**
   * Symbol Initialization
   *
   * @param scope - Reference to Vision extensible symbol definition and instance of this symbol.
   * @param elem - jQuery array containing the HTML Element of the symbol.
   */
  symbolVis.prototype.init = function (this: vision.SymbolVis<VisionSymbolConfig>, scope: VisionScope, elem: JQuery): void {
    // #region Scope Variables
    scope.LinkLocation = "#";
    scope.LinkTarget = TARGET_BLANK;
    scope.ButtonText = scope.config.ButtonText;
    // #endregion

    // #region HTML Elements
    const symbolRootElement = elem.find(`.${SYMBOL_NAME}`)[0];
    // #endregion

    // #region Base Symbol Methods
    /**
     * Process symbol data update events.
     *
     * @param newData - Object with properties determined by the symbol DataShape.
     */
    this.onDataUpdate<vision.DataShapeValue> = function (newData) {
      if (!newData) {
        return;
      }

      // If the value has changed, then update the button's HREF (location) attribute.
      if (scope.LinkLocation !== newData.Value && typeof newData.Value === "string") {
        scope.LinkLocation = newData.Value;
      }
    };

    /**
     * Process changing configuration of the symbol.
     *
     * @param newConfig - Object with the new configuration properties.
     * @param oldConfig - Object with the old configuration properties.
     */
    this.onConfigChange<VisionSymbolConfig> = function (newConfig?, oldConfig?) {
      // Ensure both configuration properties are set and not equal to each other.
      if (!newConfig || !oldConfig || angular.equals(newConfig, oldConfig)) {
        return;
      }

      // Update the button text.
      if (newConfig.ButtonText !== oldConfig.ButtonText) {
        scope.ButtonText = newConfig.ButtonText;
      }

      // Update the link's target (whether it opens in a new tab).
      if (newConfig.OpenNewTab !== oldConfig.OpenNewTab) {
        if (newConfig.OpenNewTab) {
          scope.LinkTarget = TARGET_BLANK;
        } else {
          scope.LinkTarget = TARGET_SELF;
        }
      }
    };

    /**
     * Process events where the symbol is resized.
     *
     * @param width - New width of the symbol.
     * @param height - New height of the symbol.
     */
    this.onResize = function (width, height) {
      if (symbolRootElement) {
        symbolRootElement.setAttribute("width", `${width}`);
        symbolRootElement.setAttribute("height", `${height}`);
      }
    };
    // #endregion

    // #region Styling Methods
    /**
     * Get the Main Symbol Styles
     *
     * This method builds the styles for the symbol based on its configuration.
     *
     * @returns Style properties for the symbol.
     */
    scope.getSymbolMainStyles = function () {
      return {
        "background-color": scope.config.ButtonColor,
        color: scope.config.TextColor,
        "font-size": scope.config.TextSize + "px",
        border: `${scope.config.BorderSize}px solid ${scope.config.BorderColor}`,
      };
    };
    // #endregion
  };

  PV.symbolCatalog.register(definition);
})(window.PIVisualization);
