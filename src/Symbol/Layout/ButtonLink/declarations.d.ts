export interface VisionSymbolConfig extends vision.BaseSymbolConfig {
  Height: number;
  Width: number;
  OpenNewTab: boolean;
  ButtonText: string;
  ButtonColor: string;
  BorderColor: string;
  BorderSize: number;
  TextSize: number;
  TextColor: string;
}

export interface VisionScope extends vision.BaseVisionScope<VisionSymbolConfig> {
  LinkLocation: string;
  LinkTarget: string;
  ButtonText: string;
  getSymbolMainStyles: () => { [key: string]: string };
}
