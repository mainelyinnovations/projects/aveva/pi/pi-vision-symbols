(function (PV: vision.Visualization) {
  ("use strict");

  type VisionSymbolConfig = import("./declarations").VisionSymbolConfig;
  type VisionScope = import("./declarations").VisionScope;

  // #region Symbol injected parameters
  // Define the services to inject.
  const injectedServices = ["mainelyInnovations"] as const satisfies ReadonlyArray<keyof vision.SymbolDefinitionInjectMap>;

  // Create a union type from the array
  type InjectedServiceType = (typeof injectedServices)[number];
  // #endregion

  // Base extensibility setup
  const symbolVis = function () {} as unknown as vision.SymbolVis<VisionScope, InjectedServiceType>;
  PV.deriveVisualizationFromBase(symbolVis);

  // #region Constants
  const SYMBOL_NAME = "mi-dat-timeseries-value";
  const SYMBOL_DISPLAY_NAME = "Timeseries Value";
  const SYMBOL_DEFAULT_CONFIG_KEYS = [
    "NumberRecordsShown",
    "DateStyle",
    "TimeStyle",
    "BorderRadius",
    "TableHeaderTextSize",
    "TableHeaderColor",
    "TableHeaderBackgroundColor",
    "TableHeaderTimestampAlignment",
    "TableHeaderValueAlignment",
    "TableHeaderBorderColor",
    "TableColumnSettingsColor",
    "TableSortIconColor",
    "TableRowTextSize",
    "TableRowEvenColor",
    "TableRowEvenBackgroundColor",
    "TableRowOddColor",
    "TableRowOddBackgroundColor",
    "TableRowBorderColor",
    "TableRowTimestampAlignment",
    "TableRowValueAlignment",
    "ModalHeaderColor",
    "ModalHeaderBackgroundColor",
    "ModalHeaderBorderColor",
    "ModalHeaderTextSize",
    "ModalBodyColor",
    "ModalBodyBackgroundColor",
    "ModalBodyTextSize",
    "ModalInputBorderColor",
    "ModalInputBackgroundColor",
    "ModalInputColor",
    "ModalSelectBorderColor",
    "ModalSelectBackgroundColor",
    "ModalSelectColor",
    "ModalCheckboxBorderColor",
    "ModalCheckboxBackgroundColor",
    "ModalCheckboxTickedBackgroundColor",
    "ModalFooterBackgroundColor",
    "ModalFooterBorderColor",
    "ModalFooterTextSize",
    "ModalCloseButtonColor",
    "ModalCloseButtonBackgroundColor",
    "ModalCloseButtonBorderColor",
    "ModalSaveButtonColor",
    "ModalSaveButtonBackgroundColor",
    "ModalSaveButtonBorderColor",
    "ShowLabel",
    "LabelAlignment",
    "LabelTextSize",
    "LabelColor",
    "LabelBackgroundColor",
    "LabelBorderColor",
    "ColumnSettings",
  ];
  // #endregion

  // #region Symbol Definition
  const definition: vision.SymbolDefinition<VisionSymbolConfig, VisionScope, InjectedServiceType> = {
    typeName: SYMBOL_NAME,
    displayName: SYMBOL_DISPLAY_NAME,
    visObjectType: symbolVis,
    datasourceBehavior: PV.Extensibility.Enums.DatasourceBehaviors.Single,
    iconUrl: `${PV.MainelyInnovations.symbolIconPath}/sym-${SYMBOL_NAME}.svg`,
    supportsCollections: true,
    configTemplateUrl: `${PV.MainelyInnovations.symbolBasePath}/sym-${SYMBOL_NAME}-config.html`,
    configTitle: `${PV.ResourceStrings.FormatKeyword} ${SYMBOL_DISPLAY_NAME}`,
    selectSymbolDefaultsFromConfig: PV.MainelyInnovations.selectSymbolDefaultsFromConfig(SYMBOL_DEFAULT_CONFIG_KEYS),
    configInit: PV.SymMIDatTimeseriesValue.init(),
    getDefaultConfig: function () {
      return {
        DataShape: PV.Extensibility.Enums.DataShapes.TimeSeries,
        Height: 350,
        Width: 335,
        NumberRecordsShown: 10,
        DateStyle: "medium",
        TimeStyle: "medium",
        BorderRadius: 0.3,
        TableHeaderTextSize: 12,
        TableHeaderColor: "rgba(245, 245, 245, 0.97)",
        TableHeaderBackgroundColor: "rgba(8, 31, 46, 0.18)",
        TableHeaderTimestampAlignment: "left",
        TableHeaderValueAlignment: "left",
        TableHeaderBorderColor: "rgba(8, 31, 46, 0.18)",
        TableColumnSettingsColor: "rgba(159, 175, 212, 0.83)",
        TableSortIconColor: "rgba(115, 179, 98, 0.70)",
        TableRowTextSize: 12,
        TableRowEvenColor: "rgba(10, 10, 10, 1)",
        TableRowEvenBackgroundColor: "rgba(255, 255, 255, 0.87)",
        TableRowOddColor: "rgba(10, 10, 10, 1)",
        TableRowOddBackgroundColor: "rgba(255, 255, 255, 0.97)",
        TableRowBorderColor: "rgba(179, 179, 179, 0.79)",
        TableRowTimestampAlignment: "left",
        TableRowValueAlignment: "left",
        ModalHeaderColor: "rgba(245, 245, 245, 0.87)",
        ModalHeaderBackgroundColor: "rgba(30, 30, 30, 0.95)",
        ModalHeaderBorderColor: "rgba(30, 30, 30, 0.95)",
        ModalHeaderTextSize: 20,
        ModalBodyColor: "rgba(10, 10, 10, 1)",
        ModalBodyBackgroundColor: "rgba(220, 220, 220, 0.5)",
        ModalBodyTextSize: 12,
        ModalInputBorderColor: "rgba(179, 179, 179, 0.79)",
        ModalInputBackgroundColor: "rgba(255, 255, 255, 0.97)",
        ModalInputColor: "rgba(10, 10, 10, 1)",
        ModalSelectBorderColor: "rgba(179, 179, 179, 0.79)",
        ModalSelectBackgroundColor: "rgba(255, 255, 255, 0.97)",
        ModalSelectColor: "rgba(10, 10, 10, 1)",
        ModalCheckboxBorderColor: "rgba(69, 91, 125, 0.9)",
        ModalCheckboxBackgroundColor: "rgba(255, 255, 255, 0.97)",
        ModalCheckboxTickedBackgroundColor: "rgba(69, 91, 125, 0.9)",
        ModalFooterBackgroundColor: "rgba(220, 220, 220, 0.5)",
        ModalFooterBorderColor: "rgba(179, 179, 179, 0.79)",
        ModalFooterTextSize: 12,
        ModalCloseButtonColor: "rgba(10, 10, 10, 1)",
        ModalCloseButtonBackgroundColor: "rgba(176, 176, 176, 0.69)",
        ModalCloseButtonBorderColor: "rgba(176, 176, 176, 0.69)",
        ModalSaveButtonColor: "rgba(245, 245, 245, 0.97)",
        ModalSaveButtonBackgroundColor: "rgba(30, 30, 30, 0.95)",
        ModalSaveButtonBorderColor: "rgba(30, 30, 30, 0.95)",
        ShowLabel: true,
        LabelAlignment: "center",
        LabelTextSize: 14,
        LabelColor: "rgba(245, 245, 245, 0.97)",
        LabelBackgroundColor: "rgba(8, 31, 46, 0.18)",
        LabelBorderColor: "rgba(8, 31, 46, 0.18)",
        ColumnSettings: {
          Timestamp: {
            Show: true,
            Width: 200,
          },
          Value: {
            Show: true,
            Width: 100,
          },
        },
      };
    },
    inject: injectedServices,
  };
  // #endregion

  /**
   * Symbol Initialization
   *
   * @param scope - Reference to Vision extensible symbol definition and instance of this symbol.
   * @param elem - jQuery array containing the HTML Element of the symbol.
   * @param args - Array of injected services as configured in the service definition.
   */
  symbolVis.prototype.init = function (
    this: vision.SymbolVis<VisionScope, InjectedServiceType>,
    scope: VisionScope,
    elem: JQuery,
    ...args: vision.SymbolDefinitionArgs<InjectedServiceType>
  ): void {
    // Injected parameters
    const [mainelyInnovations] = PV.MainelyInnovations.getInjectedParams(injectedServices, args);

    // #region Scope Variables
    scope.Label = "...";
    scope.Values = [];
    scope.TimestampDescending = true;
    scope.LastNumberRecordsShown = scope.config.NumberRecordsShown;
    scope.SelectedColumnSetting = angular.copy(scope.config.ColumnSettings.Timestamp);
    scope.SelectedColumnSettingName = "Timestamp";
    scope.ColumnSettingKeys = Object.keys(scope.config.ColumnSettings);
    // #endregion

    // #region HTML Elements
    const symbolRootElement = elem.find(`.${SYMBOL_NAME}`)[0];
    const symbolTableColumnSettingsElement = elem.find(`.${SYMBOL_NAME}-table-settings`)[0];
    // #endregion

    // #region Base Symbol Methods
    (function activate() {
      // Configure input events (used in the config pane and the symbol template).
      scope.runtimeData.allowOnlyNumericInput = PV.MainelyInnovations.allowOnlyNumericInput;

      // Set the formatTimestamp function on the runtime.
      scope.runtimeData.formatTimestamp = PV.MainelyInnovations.formatTimestamp;
    })();

    /**
     * Process symbol data update events.
     *
     * @param newData - Object with properties determined by the symbol DataShape.
     */
    this.onDataUpdate<vision.DataShapeTimeseries> = function (newData) {
      const dataChanges = PV.MainelyInnovations.getTimeSeriesHasDataChanges(scope.Values, newData.Data, scope.TimestampDescending);

      // Handle normal data update intervals and only update `Values` if there are changes in the dataset.
      if (dataChanges) {
        // Checking if `isNaN` ensures we are splicing by a number, in the event the user removed the keydown event handler.
        scope.Values = dataChanges[0].Values.slice(0, isNaN(scope.config.NumberRecordsShown) ? 10 : scope.config.NumberRecordsShown);
      }

      // Handle the user changing the number of records to display.
      if (scope.LastNumberRecordsShown !== scope.config.NumberRecordsShown) {
        scope.LastNumberRecordsShown = scope.config.NumberRecordsShown;
        // Make sure we get the correct order in the array before slicing it.
        scope.Values = PV.MainelyInnovations.getReversedArray(angular.copy(scope.Values), scope.TimestampDescending).slice(
          0,
          isNaN(scope.config.NumberRecordsShown) ? 10 : scope.config.NumberRecordsShown,
        );
      }
    };

    /**
     * Process events where the symbol is resized.
     *
     * @param width - New width of the symbol.
     * @param height - New height of the symbol.
     */
    this.onResize = function (width, height) {
      if (symbolRootElement) {
        // Setting these properties must be done on the `svg` HTML element to properly
        // perform a resize. The SVG (.svg) file must be defined without these properties.
        symbolRootElement.setAttribute("width", `${width}`);
        symbolRootElement.setAttribute("height", `${height}`);
      }
    };
    // #endregion

    // #region Symbol Template Methods
    /**
     * Change Timestamp Order
     *
     * This function toggles the order of timestamps in the dataset and updates the values accordingly.
     */
    scope.changeTimestampOrder = function () {
      scope.TimestampDescending = !scope.TimestampDescending;

      // Get the reversed array of values based on the updated timestamp order
      // Note: angular.copy is used to create a deep copy of the values array to prevent unintended modifications
      scope.Values = PV.MainelyInnovations.getReversedArray(angular.copy(scope.Values), scope.TimestampDescending, true);
    };

    /**
     * Change the currently selected column setting to modify
     *
     * Changes the selected column setting based on the selected column setting name.
     * If the selected column setting name is included in the ColumnSettingKeys array,
     * it updates the SelectedColumnSetting with the corresponding value from the config.
     */
    scope.changeSelectedColumnSetting = function () {
      if (scope.ColumnSettingKeys.includes(scope.SelectedColumnSettingName)) {
        scope.SelectedColumnSetting = angular.copy(scope.config.ColumnSettings[scope.SelectedColumnSettingName]);
      }
    };

    /**
     * Determine if a column setting can be saved
     *
     * Determines whether the submit button for saving column settings should be enabled or disabled.
     * The button is disabled if:
     *   - SelectedColumnSettingName does not exist in ColumnSettingKeys array
     *   - SelectedColumnSetting is equal to the corresponding value in the config
     *
     * @returns - Returns false if the setting can be saved, true (disabled) otherwise.
     */
    scope.canSaveColumnSetting = function () {
      if (
        scope.ColumnSettingKeys.includes(scope.SelectedColumnSettingName) &&
        !angular.equals(scope.SelectedColumnSetting, scope.config.ColumnSettings[scope.SelectedColumnSettingName])
      ) {
        return false;
      }

      return true;
    };

    /**
     * Saves the current column setting to the config
     *
     * If the SelectedColumnSettingName exists in the ColumnSettingKeys array and
     * the SelectedColumnSetting is different from the corresponding value in the config,
     * it updates the config with the new value.
     */
    scope.saveColumnSetting = function () {
      if (
        scope.ColumnSettingKeys.includes(scope.SelectedColumnSettingName) &&
        angular.equals(scope.SelectedColumnSetting, scope.config.ColumnSettings[scope.SelectedColumnSettingName])
      ) {
        return;
      }

      scope.config.ColumnSettings[scope.SelectedColumnSettingName] = angular.copy(scope.SelectedColumnSetting);
    };

    /**
     * Open the column settings modal.
     */
    scope.openTableColumnSettingsModal = function () {
      $(symbolTableColumnSettingsElement).addClass("show");
    };

    /**
     * Close the column settings modal.
     */
    scope.closeTableColumnSettingsModal = function () {
      $(symbolTableColumnSettingsElement).removeClass("show");
    };

    /**
     * Determine if a column should be shown
     *
     * This method checks if the provided column should be shown based on
     * the current configuration.
     *
     * @param columnType - The type of column (timestamp or value).
     * @returns Style properties for the symbol.
     */
    scope.shouldShowColumn = function (columnType: string) {
      if (Object.keys(scope.config.ColumnSettings).includes(columnType) && scope.config.ColumnSettings[columnType].Show) {
        return true;
      }

      return false;
    };
    // #endregion

    // #region Styling Methods
    /**
     * Get the Main Symbol Styles
     *
     * This method builds the styles for the symbol based on its configuration.
     *
     * @returns Style properties for the symbol.
     */
    scope.getSymbolMainStyles = function () {
      return {
        "border-radius": `${scope.config.BorderRadius}rem`,
      };
    };

    /**
     * Get the Symbol Label Styles
     *
     * This method builds the styles for the symbol's label based on its configuration.
     *
     * @returns Style properties for the label.
     */
    scope.getSymbolLabelStyles = function () {
      return {
        border: "1px solid " + scope.config.LabelBorderColor,
        "background-color": scope.config.LabelBackgroundColor,
        color: scope.config.LabelColor,
        "text-align": scope.config.LabelAlignment,
        "font-size": scope.config.LabelTextSize + "px",
      };
    };

    /**
     * Get the Symbol Header Styles
     *
     * This method builds the styles for the symbol's header based on its configuration.
     *
     * @param columnType - The type of column (timestamp or value).
     * @returns Style properties for the symbol.
     */
    scope.getSymbolHeaderStyles = function (columnType: string) {
      const columnConfigName = columnType === "Timestamp" ? "Timestamp" : "Value";

      return {
        border: "1px solid " + scope.config.TableHeaderBorderColor,
        "background-color": scope.config.TableHeaderBackgroundColor,
        color: scope.config.TableHeaderColor,
        "font-size": scope.config.TableHeaderTextSize + "px",
        "text-align": scope.config[`TableHeader${columnConfigName}Alignment`],
        width: scope.config.ColumnSettings[columnConfigName].Width + "px",
      };
    };

    /**
     * Get the Symbol Column Settings Styles
     *
     * This method builds the styles for the symbol's column settings button based on its configuration.
     *
     * @returns Style properties for the symbol.
     */
    scope.getSymbolColumnSettingStyles = function () {
      return {
        "margin-right": "5px",
        color: scope.config.TableColumnSettingsColor,
      };
    };

    /**
     * Get the Symbol Sort Icon Styles
     *
     * This method builds the styles for the symbol's sort icon based on its configuration.
     *
     * @returns Style properties for the sort icon.
     */
    scope.getSymbolSortIconStyles = function () {
      return {
        color: scope.config.TableSortIconColor,
      };
    };

    /**
     * Get the Symbol Row Styles
     *
     * This method builds the styles for the symbol's time series rows based on its configuration.
     *
     * @param rowType - The type of row (even or odd).
     * @param columnType - The type of column (timestamp or value).
     * @returns Style properties for the row type.
     */
    scope.getSymbolRowStyles = function (rowType: string, columnType: string) {
      const rowConfigName = rowType === "even" ? "Even" : "Odd";
      const columnConfigName = columnType === "timestamp" ? "Timestamp" : "Value";

      return {
        border: "1px solid " + scope.config.TableRowBorderColor,
        "background-color": scope.config[`TableRow${rowConfigName}BackgroundColor`],
        color: scope.config[`TableRow${rowConfigName}Color`],
        "font-size": scope.config.TableRowTextSize + "px",
        "text-align": scope.config[`TableRow${columnConfigName}Alignment`],
      };
    };

    /**
     * Get Modal Section Styles
     *
     * This method gets the styles for the different modal sections.
     *
     * @param section - Section of the modal to get styles for.
     * @returns Object of style properties for ng-style.
     */
    scope.getModalSectionStyles = function (section: string) {
      if (section === "header") {
        return {
          "border-bottom": "1px solid " + scope.config.ModalHeaderBorderColor,
          "background-color": scope.config.ModalHeaderBackgroundColor,
          color: scope.config.ModalHeaderColor,
          "font-size": scope.config.ModalHeaderTextSize + "px",
        };
      }

      if (section === "body") {
        return {
          "background-color": scope.config.ModalBodyBackgroundColor,
          color: scope.config.ModalBodyColor,
          "font-size": scope.config.ModalBodyTextSize + "px",
        };
      }

      if (section === "footer") {
        return {
          "border-top": "1px solid " + scope.config.ModalFooterBorderColor,
          "background-color": scope.config.ModalFooterBackgroundColor,
        };
      }
    };

    /**
     * Get Input Styles
     *
     * This method gets the styles for the form inputs.
     *
     * @returns Object of style properties for ng-style.
     */
    scope.getFormInputStyles = function () {
      return {
        border: "1px solid " + scope.config.ModalInputBorderColor,
        "background-color": scope.config.ModalInputBackgroundColor,
        color: scope.config.ModalInputColor,
      };
    };

    /**
     * Get Select Styles
     *
     * This method gets the styles for the form selects.
     *
     * @returns Object of style properties for ng-style.
     */
    scope.getFormSelectStyles = function () {
      return {
        border: "1px solid " + scope.config.ModalSelectBorderColor,
        "background-color": scope.config.ModalSelectBackgroundColor,
        color: scope.config.ModalSelectColor,
      };
    };

    /**
     * Get Checkbox Styles
     *
     * This method gets the styles for the form checkboxes.
     *
     * @param isChecked -  Whether the checkbox is checked or not.
     * @returns Object of style properties for ng-style.
     */
    scope.getFormCheckboxStyles = function (isChecked: boolean) {
      return {
        "border-color": scope.config.ModalCheckboxBorderColor,
        "background-color": isChecked ? scope.config.ModalCheckboxTickedBackgroundColor : scope.config.ModalCheckboxBackgroundColor,
      };
    };

    /**
     * Get Button Styles
     *
     * This method gets the styles for the available Buttons.
     *
     * @returns Object of style properties for ng-style.
     */
    scope.getButtonStyles = function (buttonType: string) {
      const buttonConfigName = buttonType === "close" ? "Close" : "Save";

      return {
        border: "1px solid " + scope.config[`Modal${buttonConfigName}ButtonBorderColor`],
        "background-color": scope.config[`Modal${buttonConfigName}ButtonBackgroundColor`],
        color: scope.config[`Modal${buttonConfigName}ButtonColor`],
        "font-size": scope.config.ModalFooterTextSize + "px",
      };
    };
    // #endregion

    // #region Symbol Service Setup
    mainelyInnovations.updateSymbolSingleLabel(this);
    // #endregion
  };

  PV.symbolCatalog.register(definition);
})((window as any).PIVisualization);
