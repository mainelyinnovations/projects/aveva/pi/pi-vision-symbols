export interface VisionSymbolConfig extends vision.BaseSymbolConfig {
  Height: number;
  Width: number;
  NumberRecordsShown: number;
  DateStyle: string;
  TimeStyle: string;
  BorderRadius: number;
  TableHeaderTextSize: number;
  TableHeaderColor: string;
  TableHeaderBackgroundColor: string;
  TableHeaderBorderColor: string;
  TableHeaderTimestampAlignment: string;
  TableHeaderValueAlignment: string;
  TableColumnSettingsColor: string;
  TableSortIconColor: string;
  TableRowTextSize: number;
  TableRowEvenColor: string;
  TableRowEvenBackgroundColor: string;
  TableRowOddColor: string;
  TableRowOddBackgroundColor: string;
  TableRowBorderColor: string;
  TableRowTimestampAlignment: string;
  TableRowValueAlignment: string;
  ModalHeaderColor: string;
  ModalHeaderBackgroundColor: string;
  ModalHeaderBorderColor: string;
  ModalHeaderTextSize: number;
  ModalBodyColor: string;
  ModalBodyBackgroundColor: string;
  ModalBodyTextSize: number;
  ModalInputBorderColor: string;
  ModalInputBackgroundColor: string;
  ModalInputColor: string;
  ModalSelectBorderColor: string;
  ModalSelectBackgroundColor: string;
  ModalSelectColor: string;
  ModalCheckboxBorderColor: string;
  ModalCheckboxBackgroundColor: string;
  ModalCheckboxTickedBackgroundColor: string;
  ModalFooterBackgroundColor: string;
  ModalFooterBorderColor: string;
  ModalFooterTextSize: number;
  ModalCloseButtonColor: string;
  ModalCloseButtonBackgroundColor: string;
  ModalCloseButtonBorderColor: string;
  ModalSaveButtonColor: string;
  ModalSaveButtonBackgroundColor: string;
  ModalSaveButtonBorderColor: string;
  ShowLabel: boolean;
  LabelAlignment: string;
  LabelTextSize: number;
  LabelColor: string;
  LabelBackgroundColor: string;
  LabelBorderColor: string;
  ColumnSettings: { [key: string]: { Show: boolean; Width: number } };
}

export interface RunTimeData extends vision.BaseSymbolRuntimeData<VisionSymbolConfig> {
  formatTimestamp: (dateTime: Date, dateStyle: DateTimeStyle, timeStyle: DateTimeStyle) => Date | string;
  allowOnlyNumericInput: (event: KeyboardEvent) => void;
}

export interface VisionScope extends vision.BaseVisionScope<VisionSymbolConfig, RunTimeData> {
  Label: string;
  Values: Array<vision.PIValue>;
  TimestampDescending: boolean;
  LastNumberRecordsShown: number;
  SelectedColumnSetting: { Show: boolean; Width: number };
  SelectedColumnSettingName: string;
  ColumnSettingKeys: Array<string>;
  changeTimestampOrder: () => void;
  changeSelectedColumnSetting: () => void;
  canSaveColumnSetting: () => boolean;
  saveColumnSetting: () => void;
  openTableColumnSettingsModal: () => void;
  closeTableColumnSettingsModal: () => void;
  shouldShowColumn: (columnType: string) => boolean;
  getSymbolMainStyles: () => { [key: string]: string };
  getSymbolLabelStyles: () => { [key: string]: string };
  getSymbolHeaderStyles: (columnType: string) => { [key: string]: string };
  getSymbolColumnSettingStyles: () => { [key: string]: string };
  getSymbolSortIconStyles: () => { [key: string]: string };
  getSymbolRowStyles: (rowType: string, columnType: string) => { [key: string]: string };
  getModalSectionStyles: (section: string) => { [key: string]: string };
  getFormInputStyles: () => { [key: string]: string };
  getFormSelectStyles: () => { [key: string]: string };
  getFormCheckboxStyles: (isChecked: boolean) => { [key: string]: string };
  getButtonStyles: (buttonType: string) => { [key: string]: string };
}

export interface SymbolConfigInit {
  init: () => (scope: VisionScope) => void;
}
