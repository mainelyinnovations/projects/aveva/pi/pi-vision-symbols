(function (PV: vision.Visualization) {
  "use strict";

  // Types
  type VisionScope = import("./declarations").VisionScope;

  PV.SymMIDatTimeseriesValue = (function () {
    /**
     * Perform configuration initialization for the Timeseries Value symbol.
     *
     * - Initializing label settings.
     *
     * @param options - Options for configuration initialization.
     */
    function init() {
      /**
       * Function passed to `configInit` in symbol definitions.
       *
       * @param scope Reference to Vision extensible symbol definition and instance of the provided symbol.
       */
      return function (scope: VisionScope): void {
        // Configure label settings.
        PV.MainelyInnovations.configInit({ labelSettingsInit: true })(scope);
      };
    }

    return {
      init,
    };
  })();
})(window.PIVisualization);
