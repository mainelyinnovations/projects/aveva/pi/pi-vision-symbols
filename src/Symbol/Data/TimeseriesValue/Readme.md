# Timeseries Value

The Timeseries Value displays the archived values for a singular record for the time duration of the display. The symbol can be configured to reduce the number of records to a fixed amount and can be sorted by timestamp ascending or descending.

## Requirements

- Bootstrap CSS
- Font Awesome

## Symbol Information

- Supports Collections: `true`
- Supports Default Configuration: `true`
- DataShape: `Timeseries`
  - This symbol is a single data source with all archived values between the start & end time of the display.
- DatasourceBehavior: `Single`
  - The symbol is designed to work with a single data source, either a PI Point or an AF Attribute.

## Config

### Core Options

- Number of Records
  - Defines the number of records shown in the symbol.
- Date Style
  - Defines the display style for the date portion of timestamps.
- Time Style
  - Defines the display style for the time portion of timestamps.
- Border Radius
  - Defines the border radius of the symbol.

### Table Header Style

- Header Text Size
  - Sets the size for text within the table header.
- Header Text
  - Defines the color of the table header text.
- Header BG
  - Defines the color of the table header background.
- Header Border
  - Defines the color of the table header border.
- Time Alignment
  - Sets the alignment to the left, center, or right for the `Timestamp` table header column.
- Value Alignment
  - Sets the alignment to the left, center, or right for the `Value` table header column.
- Column Settings
  - Defines the color column settings configuration button.
- Sort Icon
  - Defines the color of the sort icon button.

### Table Row Style

- Row Text Size
  - Sets the size for text within the body rows of the table.
- Even Row Text
  - Defines the color of even numbered row text.
- Even Row BG
  - Defines the background color of even numbered rows.
- Odd Row Text
  - Defines the color of odd numbered row text.
- Odd Row BG
  - Defines the background color of odd numbered rows.
- Row Border
  - Defines the color of the borders for body rows of the table.
- Time Alignment
  - Sets the alignment to the left, center, or right for the `Timestamp` body column's rows.
- Value Alignment
  - Sets the alignment to the left, center, or right for the `Value` body column's rows.

### Modal Style

- Header Text Size
  - Sets the size for text within the modal header.
- Header Text
  - Defines the color of the modal header text.
- Header BG
  - Defines the color of the modal header background.
- Header Border
  - Defines the color of the modal header border.
- Body Text
  - Defines the color of the modal body text.
- Body BG
  - Defines the color of the modal body background.
- Body Text Size
  - Sets the size for text within the modal body.
- Input Border
  - Defines the border color of form inputs.
- Input BG
  - Defines the background color of form inputs.
- Input Text
  - Defines the text color of form inputs.
- Select Border
  - Defines the border color of form selects.
- Select BG
  - Defines the background color of form selects.
- Select Text
  - Defines the text color of form selects.
- Checkbox Border
  - Defines the border color of form checkboxes.
- Checkbox BG
  - Defines the background color of form checkboxes.
- Checkbox Ticked BG
  - Defines the background color of selected form checkboxes.
- Footer BG
  - Defines the color of the modal footer background.
- Footer Border
  - Defines the color of the modal footer border.
- Footer Text Size
  - Sets the size for text within the modal footer.
- Close Button Text
  - Defines the text color of the modal close button.
- Close Button BG
  - Defines the background color of the modal close button.
- Close Button Border
  - Defines the border color of the modal close button.
- Save Button Text
  - Defines the text color of the modal save button.
- Save Button BG
  - Defines the background color of the modal save button.
- Save Button Border
  - Defines the border color of the modal save button.

### Label

- Show Label
  - Determines if a label will be shown for the symbol.
- Label
  - Defines the PI Point, portion of the AF attribute path, or custom text to display for the label.
- Label Alignment
  - Defines the text alignment for the label.
- Label Text Size
  - Defines the text size for the label.
- Label Text
  - Defines the text color for the label.
- Label BG
  - Defines the background color for the label.
- Label Border
  - Defines the border color for the label.

### Symbol UI

These configuration items are controlled through the UI of the symbol.

- Column Settings
  - For each column (timestamp & value) available to the symbol, the following options are exposed:
    - Show
      - Defines whether the column will be shown or hidden.
    - Width
      - Defines the width in pixels for the column.
