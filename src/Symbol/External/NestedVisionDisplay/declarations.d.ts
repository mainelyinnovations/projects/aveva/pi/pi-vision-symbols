export interface VisionSymbolConfig extends vision.BaseSymbolConfig {
  Height: number;
  Width: number;
  BorderColor: string;
  BorderSize: number;
  ShowLabel: boolean;
  LabelAlignment: string;
  TextSize: number;
  TextColor: string;
}

export interface VisionScope extends vision.BaseVisionScope<VisionSymbolConfig> {
  StartTime: string;
  EndTime: string;
  Label: string;
  Value?: string;
  HasTimeChanged?: boolean;
  HasValueChanged?: boolean;
  getSymbolMainStyles: () => { [key: string]: string };
  getSymbolLabelStyles: () => { [key: string]: string };
}
