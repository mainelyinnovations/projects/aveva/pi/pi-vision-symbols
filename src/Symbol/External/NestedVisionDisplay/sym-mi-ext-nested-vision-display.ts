(function (PV: vision.Visualization) {
  ("use strict");

  // Import types
  type VisionSymbolConfig = import("./declarations").VisionSymbolConfig;
  type VisionScope = import("./declarations").VisionScope;

  // #region Symbol injected parameters
  // Define the services to inject.
  const injectedServices = ["timeProvider", "mainelyInnovations"] as const satisfies ReadonlyArray<keyof vision.SymbolDefinitionInjectMap>;

  // Create a union type from the array
  type InjectedServiceType = (typeof injectedServices)[number];
  // #endregion

  // Base extensibility setup
  const symbolVis = function () {} as unknown as vision.SymbolVis<VisionScope, InjectedServiceType>;
  PV.deriveVisualizationFromBase(symbolVis);

  // #region Constants
  const SYMBOL_NAME = "mi-ext-nested-vision-display";
  const SYMBOL_DISPLAY_NAME = "Nested Vision Display";
  const SYMBOL_DEFAULT_CONFIG_KEYS = ["BorderColor", "BorderSize", "ShowLabel", "LabelAlignment", "TextSize", "TextColor"];
  // #endregion

  // #region Symbol Definition
  const definition: vision.SymbolDefinition<VisionSymbolConfig, VisionScope, InjectedServiceType> = {
    typeName: SYMBOL_NAME,
    displayName: SYMBOL_DISPLAY_NAME,
    visObjectType: symbolVis,
    datasourceBehavior: PV.Extensibility.Enums.DatasourceBehaviors.Single,
    iconUrl: `${PV.MainelyInnovations.symbolIconPath}/sym-${SYMBOL_NAME}.svg`,
    supportsCollections: false,
    configTemplateUrl: `${PV.MainelyInnovations.symbolBasePath}/sym-${SYMBOL_NAME}-config.html`,
    configTitle: `${PV.ResourceStrings.FormatKeyword} ${SYMBOL_DISPLAY_NAME}`,
    selectSymbolDefaultsFromConfig: PV.MainelyInnovations.selectSymbolDefaultsFromConfig(SYMBOL_DEFAULT_CONFIG_KEYS),
    configInit: PV.MainelyInnovations.configInit({ labelSettingsInit: true }),
    getDefaultConfig: function () {
      return {
        DataShape: PV.Extensibility.Enums.DataShapes.Value,
        Height: 500,
        Width: 700,
        BorderColor: "rgba(65, 72, 83, 0.95)",
        BorderSize: 1,
        ShowLabel: true,
        LabelAlignment: "center",
        TextSize: 16,
        TextColor: "rgba(255, 255, 255, 1)",
      };
    },
    inject: injectedServices,
  };
  // #endregion

  /**
   * Symbol Initialization
   *
   * @param scope - Reference to Vision extensible symbol definition and instance of this symbol.
   * @param elem - jQuery array containing the HTML Element of the symbol.
   * @param args - Array of injected services as configured in the service definition.
   */
  symbolVis.prototype.init = function (
    this: vision.SymbolVis<VisionScope, InjectedServiceType>,
    scope: VisionScope,
    elem: JQuery,
    ...args: vision.SymbolDefinitionArgs<InjectedServiceType>
  ): void {
    // Injected parameters
    const [timeProvider, mainelyInnovations] = PV.MainelyInnovations.getInjectedParams(injectedServices, args);

    // #region Scope Variables
    scope.StartTime = "*-8h";
    scope.EndTime = "*";
    scope.Label = "...";
    scope.Value = "";
    // #endregion

    // #region HTML Elements
    const symbolRootElement = elem.find(`.${SYMBOL_NAME}`);
    const symbolIFrameElement = symbolRootElement?.find("iframe")[0] || undefined;
    // #endregion

    // #region Base Symbol Methods
    /**
     * Process symbol data update events.
     *
     * @param newData - Object with properties determined by the symbol DataShape.
     */
    this.onDataUpdate<vision.DataShapeValue> = function (newData) {
      if (!newData) {
        return;
      }

      // Check if the display time has changed.
      scope.HasTimeChanged = PV.MainelyInnovations.hasDisplayTimeChanged(scope, timeProvider);
      // Check if the data source value has changed.
      scope.HasValueChanged = hasValueChanged(newData.Value);

      // Update the nested display if either the display time or the value has changed.
      if (scope.HasTimeChanged || scope.HasValueChanged) {
        updatedNestedDisplay();
      }
    };

    /**
     * Process events where the symbol is resized.
     *
     * @param width - New width of the symbol.
     * @param height - New height of the symbol.
     */
    this.onResize = function (width, height) {
      if (symbolIFrameElement) {
        symbolIFrameElement.setAttribute("width", `${width}`);
        symbolIFrameElement.setAttribute("height", `${height}`);
      }
    };
    // #endregion

    // #region Styling Methods
    /**
     * Get the Main Symbol Styles
     *
     * This method builds the styles for the symbol based on its configuration.
     *
     * @returns Style properties for the symbol.
     */
    scope.getSymbolMainStyles = function () {
      return {
        border: `${scope.config.BorderSize}px solid ${scope.config.BorderColor}`,
      };
    };

    /**
     * Get the Symbol Label Styles
     *
     * This method builds the styles for the symbol's label based on its configuration.
     *
     * @returns Style properties for the label.
     */
    scope.getSymbolLabelStyles = function () {
      return {
        color: scope.config.TextColor,
        "text-align": scope.config.LabelAlignment,
        "font-size": scope.config.TextSize + "px",
      };
    };
    // #endregion

    // #region Symbol Service Setup
    mainelyInnovations.updateSymbolSingleLabel(this);
    // #endregion

    // #region Symbol Helper Methods
    /**
     * Checks if the provided new value is different from the current value in the scope,
     * and if the new value is a string. If the conditions are met, updates the scope's value
     * and returns true, indicating that the value has changed.
     *
     * @param newValue - The new value as provided by the Vision update service.
     * @returns True if the value has changed and updated, otherwise false.
     */
    function hasValueChanged(newValue: vision.DataUpdateSingleValue) {
      let valueChanged = false;

      // Check if the new value is different from the current value in the scope
      // and if the new value is a string (required for a proper link).
      if (scope.Value !== newValue && typeof newValue === "string") {
        scope.Value = newValue;
        valueChanged = true;
      }

      return valueChanged;
    }

    /**
     * Update the URL of the nested Vision Display to reflect changes in the base
     * display URL (due to a modified data source value) or to accommodate new
     * display start and end times. It is crucial to update the start and end times
     * of the nested display to ensure that the data aligns with the time range of
     * the display where the symbol is implemented.
     */
    function updatedNestedDisplay() {
      try {
        // Setup a URL variable, but remove the hash routing nomenclature
        // so that we can correctly parse the query parameters.
        const url = new URL(scope.Value.replace("#/", ""));

        // Set the nested display's start & end time.
        url.searchParams.set("starttime", scope.StartTime);
        url.searchParams.set("endtime", scope.EndTime);

        // Replace the hash routing to pull in the display correctly.
        url.href = url.href.replace("PIVision/Displays", "PIVision/#/Displays");

        if (symbolIFrameElement) {
          // Update the src attribute of the symbol container element with the modified URL.
          symbolIFrameElement.setAttribute("src", url.toString());
        }
      } catch {
        console.error(`[${SYMBOL_NAME}] Invalid URL : ${scope.Value}`);
      }
    }
    // #endregion
  };

  PV.symbolCatalog.register(definition);
})(window.PIVisualization);
