# Nested Vision Display

The Nested Vision Display Symbol is utilized to embed an iFrame of one PI Vision Display within a another display. This allows for the seamless integration of dynamic content from one display into another, sharing the same time context.

This symbol proves particularly useful for showcasing dynamic content on asset-relative displays. For instance, if you aim to display diagrams that vary for each asset while using the same element template, the Nested Vision Display Symbol streamlines the process, reducing the effort required to share different diagrams on a single PI Vision display.

## Symbol Definition

- Supports Collections: `false`
- Supports Default Configuration: `true`
- DataShape: `Value`
  - This symbol utilizes a single value for a given datasource at a specific point in time.
- Datasource Behavior: `Single`
  - The symbol is designed to work with a single data source, either a PI Point or an AF Attribute.

## Datasource Configuration

The table below outlines highly recommended query parameters for the PI Vision display URL used with this symbol. Experiment with the configuration to tailor it to your specific requirements.

| Parameter     | Value   |
| ------------- | ------- |
| `mode`        | `kiosk` |
| `hidetoolbar` | `true`  |
| `hidetimebar` | `true`  |
| `hidesidebar` | `true`  |

## Symbol Config

### Styling

- Border Color
  - Determines the color used for the border of the iFrame.
  - _note_: Consider implementing the border on the template used for the assets to ensure consistent displays when utilized with an asset-relative display.
- Border Size
  - Sets the border size (width) for the iFrame.
- Text Size
  - Sets the size for all fonts used within the symbol.
- Label Color
  - Defines the color of the label text.

### Visibility

- Show Label
  - Determines if a label will be shown for the symbol.
- Label
  - Defines the PI Point, portion of the AF attribute path, or custom text to display for the label.
- Label Alignment
  - Defines the text alignment for the label.
