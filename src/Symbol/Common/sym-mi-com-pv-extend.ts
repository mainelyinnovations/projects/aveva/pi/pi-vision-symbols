(function (PV: vision.Visualization) {
  ("use strict");

  // Configure a symbol DataShape enum if not already defined.
  if (!getNested(PV, "Extensibility", "Enums", "DataShapes")) {
    PV.Extensibility.Enums.DataShapes = {
      Value: "Value" as vision.DataShape.Value,
      Gauge: "Gauge" as vision.DataShape.Gauge,
      Trend: "Trend" as vision.DataShape.Trend,
      Table: "Table" as vision.DataShape.Table,
      TimeSeries: "TimeSeries" as vision.DataShape.TimeSeries,
    };
  }

  // Array of resource strings to add if they don't already exist.
  // Index 0 - Resource string key
  // Index 1 - Value for the given key
  const resourceStrings = [
    ["BodyBackgroundColor", "Body BG"],
    ["BodyBorderColor", "Body Border"],
    ["BodyColor", "Body Text"],
    ["BodyTextSize", "Body Text Size"],
    ["BorderColor", "Border"],
    ["BorderRadius", "Border Radius"],
    ["BorderSize", "Border Size"],
    ["ButtonBorderColor", "Button Border"],
    ["ButtonColor", "Button Text"],
    ["ButtonText", "Button Text"],
    ["CheckboxBackgroundColor", "Checkbox BG"],
    ["CheckboxBorderColor", "Checkbox Border"],
    ["CheckboxTickedBackgroundColor", "Checkbox Ticked BG"],
    ["CloseButtonColor", "Close Button Text"],
    ["CloseButtonBackgroundColor", "Close Button BG"],
    ["CloseButtonBorderColor", "Close Button Border"],
    ["CoreOptions", "Core Options"],
    ["DateStyle", "Date Style"],
    ["FooterBackgroundColor", "Footer BG"],
    ["FooterBorderColor", "Footer Border"],
    ["FooterColor", "Footer Text"],
    ["FooterTextSize", "Footer Text Size"],
    ["FormatKeyword", "Format"],
    ["HeaderBackgroundColor", "Header BG"],
    ["HeaderBorderColor", "Header Border"],
    ["HeaderColor", "Header Text"],
    ["HeaderTextSize", "Header Text Size"],
    ["LabelAlignment", "Label Alignment"],
    ["LabelBackgroundColor", "Label BG"],
    ["LabelBorderColor", "Label Border"],
    ["LabelColor", "Label Text"],
    ["LabelTextSize", "Label Text Size"],
    ["InputBackgroundColor", "Input BG"],
    ["InputBorderColor", "Input Border"],
    ["InputColor", "Input Text"],
    ["ModalStyleKeyword", "Modal Style"],
    ["NumberOfRecords", "Number Of Records"],
    ["OpenInNewTab", "Open in new tab"],
    ["SaveButtonColor", "Save Button Text"],
    ["SaveButtonBackgroundColor", "Save Button BG"],
    ["SaveButtonBorderColor", "Save Button Border"],
    ["SelectBackgroundColor", "Select BG"],
    ["SelectBorderColor", "Select Border"],
    ["SelectColor", "Select Text"],
    ["SortIconColor", "Sort Icon"],
    ["TextColor", "Text Color"],
    ["TableHeaderStyleKeyword", "Table Header Style"],
    ["TableRowBorderColor", "Row Border"],
    ["TableRowEvenTextColor", "Even Row Text"],
    ["TableRowEvenBackgroundColor", "Even Row BG"],
    ["TableRowOddTextColor", "Odd Row Text"],
    ["TableRowOddBackgroundColor", "Odd Row BG"],
    ["TableRowStyleKeyword", "Table Row Style"],
    ["TableRowTextSize", "Row Text Size"],
    ["TableColumnSettingsColor", "Column Settings"],
    ["TimeAlignment", "Time Alignment"],
    ["TimeStyle", "Time Style"],
    ["ValueAlignment", "Value Alignment"],
    ["ValueConfigDateTimeStyleShort", "Short"],
    ["ValueConfigDateTimeStyleMedium", "Medium"],
    ["ValueConfigDateTimeStyleLong", "Long"],
    ["ValueConfigDateTimeStyleFull", "Full"],
    ["Vector", "Vector"],
    ["VectorColor", "Vector Color"],
  ];

  // For each set of resource strings, check if the key exists
  // and add the key-value pair if it does not exist.
  resourceStrings.forEach(function (resourceString) {
    if (!PV.ResourceStrings[resourceString[0]]) {
      PV.ResourceStrings[resourceString[0]] = resourceString[1];
    }
  });

  /**
   * Searches for nested keys within an object and checks for their existence.
   *
   * @param obj - The object to search for defined keys.
   * @param args - An array of keys to check for existence in the provided object.
   * @returns A boolean indicating whether all specified keys exist in the object's nested structure.
   */
  function getNested(obj: vision.Visualization, ...args: Array<string>) {
    return args.reduce((obj, level) => obj && obj[level], obj);
  }
})(window.PIVisualization);
