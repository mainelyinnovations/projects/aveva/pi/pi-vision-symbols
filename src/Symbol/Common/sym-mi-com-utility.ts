(function (PV: vision.Visualization) {
  ("use strict");

  /**
   * NOTES:
   * This file defines a custom AngularJS service and interface for Mainely Innovations,
   * intended to be used within custom symbols. The custom service facilitates
   * the interaction with other injected services without the need for explicit arguments
   * when working with symbols.
   */

  // #region Mainely Innovations Service
  // Create a custom Mainely Innovations service to expose functionality.
  angular.module(window.APPNAME).factory("mainelyInnovations", ["labelUpdateService", mainelyInnovations]);

  /**
   * Factory function for the MainelyInnovations service, which provides various common functions from
   * Mainely Innovations.
   *
   * @param labelUpdateService - Vision Label Update Service
   * @returns An instance of the MainelyInnovations service.
   */
  function mainelyInnovations(labelUpdateService: vision.InjectLabelUpdateService) {
    return {
      updateSymbolSingleLabel,
    };

    /**
     * Configure the label update service for a symbol to handle properly
     * updating labels when the asset, data, or label configuration changes.
     *
     * @param that - Instance of a symbol.
     * @param callback - Callback function to handle label changes.
     */
    function updateSymbolSingleLabel<S extends vision.SymbolScopeHasLabel, I extends keyof vision.SymbolDefinitionInjectMap = never>(
      that: vision.SymbolVis<S, I>,
      callback?: <C = void>(label: string) => C,
    ) {
      PV.MainelyInnovations.updateSymbolSingleLabel(that, labelUpdateService, callback);
    }
  }
  // #endregion

  // #region Mainely Innovations Interface
  /**
   * Configures a global Mainely Innovations helper utility that can be used
   * across symbols for common tasks.
   */
  PV.MainelyInnovations = (function () {
    const symbolBasePath = "scripts/app/editor/symbols/ext";
    const symbolIconPath = `${symbolBasePath}/icons`;

    /**
     * Perform configuration initialization for symbols. Possible actions include:
     *
     * - Initializing label settings.
     * - Adding resource strings for text.
     *
     * @param options - Options for configuration initialization.
     */
    function configInit(options?: MainelyInnovationsConfigInitOptions) {
      /**
       * Function passed to `configInit` in symbol definitions.
       *
       * @param scope Reference to Vision extensible symbol definition and instance of the provided symbol.
       */
      return function <S extends vision.BaseVisionScope<vision.BaseSymbolConfig, vision.BaseSymbolRuntimeData<vision.BaseSymbolConfig>>>(
        scope: S,
      ): void {
        // Initialize the label settings for the symbol configuration.
        if (options.labelSettingsInit) {
          PV.labelSettingsInit(scope);
        }
      };
    }

    /**
     * Retrieve the injected services, providers, or other classes passed into a symbol initialization
     * function (as defined through the symbol's configuration `inject` property).
     *
     * @param inject - Array of services (string values) to inject as defined by the keys in `SymbolDefinitionInjectMap`.
     * @param args - Array of arguments passed to the `symbol.prototype.init` function (comes after the `scope` & `elem` parameters)
     * @returns Array of services injected to the `init` function for the symbol, or undefined for the index if the service was not found.
     */
    function getInjectedParams<T extends ReadonlyArray<keyof vision.SymbolDefinitionInjectMap>>(
      inject: T,
      args: vision.SymbolDefinitionArgs<T[number]>,
    ): vision.InjectedServices<T> {
      return inject.map((key, index) => {
        // Return the service from the arguments based on the injected index, or undefined
        // if the index is not found.
        return index !== -1 ? args[index] : undefined;
      }) as vision.InjectedServices<T>;
    }

    /**
     * Select symbol default configuration settings
     *
     * Returns a configuration object for the symbol containing configuration of the allowed keys.
     *
     * @param allowedKeys - Array of allowed symbol configuration keys to be loaded from the default configuration.
     * @returns Configuration object for the symbol containing configuration of the allowed keys.
     */
    function selectSymbolDefaultsFromConfig(allowedKeys: Array<string>) {
      return function <C>(config: C) {
        // Filter out the keys from the provided configuration that are not in the allowedKeys array.
        const defaults = Object.keys(config)
          .filter((key) => allowedKeys.includes(key))
          // Create a new object containing only the allowed keys and their corresponding values.
          .reduce(function (obj, key) {
            obj[key] = config[key];
            return obj;
          }, {}) as C;

        return defaults;
      };
    }

    /**
     * Checks if the display start or end time in the scope has changed compared to the
     * corresponding values in the time provider. If changes are detected, updates the
     * scope's start and end times accordingly and returns true.
     *
     * @returns True if the display start or end time has changed and updated, otherwise false.
     */
    function hasDisplayTimeChanged<S extends vision.SymbolScopeHasStartEndTime>(scope: S, timeProvider: vision.InjectTimeProvider): boolean {
      const displayStartTime = timeProvider.getDisplayStartTime();
      const displayEndTime = timeProvider.getDisplayEndTime();
      let hasTimeChanged = false;

      if (scope.StartTime !== displayStartTime) {
        scope.StartTime = displayStartTime;
        hasTimeChanged = true;
      }

      if (scope.EndTime !== displayEndTime) {
        scope.EndTime = displayEndTime;
        hasTimeChanged = true;
      }

      return hasTimeChanged;
    }

    /**
     * Configure the label update service for a symbol to handle properly
     * updating labels when the asset, data, or label configuration changes.
     *
     * @param that - Instance of a symbol.
     * @param labelUpdateService - An instance of the label update service.
     * @param callback - Callback function to handle label changes.
     */
    function updateSymbolSingleLabel<S extends vision.SymbolScopeHasLabel, I extends keyof vision.SymbolDefinitionInjectMap = never>(
      that: vision.SymbolVis<S, I>,
      labelUpdateService: vision.InjectLabelUpdateService,
      callback?: <C = void>(label: string) => C,
    ) {
      // Choose the callback function based on whether a callback function was provided by the symbol.
      const functionToCall = callback ? callback : noFunctionProvided;

      // The label update service does not perform error handling when calling the `onConfigChange` method.
      // Therefore, if the method is not defined on the symbol's scope, declare an empty function to prevent
      // error messages in the console logs.
      if (!that.onConfigChange) {
        that.onConfigChange<vision.BaseSymbolConfig> = function (newConfig?, oldConfig?) {};
      }

      // Call the label update service for the provided symbol and the chosen callback function.
      labelUpdateService.init(that, functionToCall);

      /**
       * Update symbol scope to set the label equal to the updated value
       * provided by the label update service.
       *
       * @param label Label returned from the label update service.
       */
      function noFunctionProvided(label: string) {
        if (that.scope.Label !== label) {
          that.scope.Label = label;
        }
      }
    }

    /**
     * Safely retrieves a color by checking if it is valid. If the provided color is valid, it is returned;
     * otherwise, the fallback color is returned.
     *
     * @param color - The color to be checked.
     * @param fallback - The fallback color to be used if the provided color is invalid.
     * @returns Either the provided color if it's valid or the fallback color.
     */
    function getSafeColor(color: string, fallback: string) {
      return PV.Utils.isSafeColor(color) ? color : fallback;
    }

    /**
     * Retrieves the hexadecimal representation of a color string. If the color string already starts with '#',
     * it's considered as a hexadecimal representation and returned as is. Otherwise, it's assumed to be an RGB
     * color string and converted to hexadecimal.
     *
     * @param colorString - The color string to convert.
     * @returns The hexadecimal representation of the color.
     */
    function getHexFromRGB(colorString: string) {
      if (colorString.startsWith("#")) {
        return colorString;
      }

      return parseColor(colorString).hex;
    }

    /**
     * Parses a color string and returns its hexadecimal representation along with its opacity.
     *
     * @param color - The color string to parse.
     * @returns An object containing the hexadecimal representation of the color and its opacity.
     */
    function parseColor(color: string) {
      /**
       * Converts an integer to its hexadecimal representation.
       *
       * @param int - The integer to convert.
       * @returns The hexadecimal representation of the integer.
       */
      function toHex(int: number) {
        const hex = int.toString(16);
        return hex.length == 1 ? "0" + hex : hex;
      }

      const arr = color.split(/[\d+.]+/g);
      const processedArr = arr.map((v) => {
        return parseFloat(v);
      });

      return {
        hex: "#" + processedArr.slice(0, 3).map(toHex).join(""),
        opacity: arr.length == 4 ? arr[3] : 1,
      };
    }

    /**
     * Get Reversed Array
     *
     * This function takes an array as input and conditionally reverses it based on input flags.
     *
     * @param input - The input array.
     * @param shouldReverse - A boolean flag indicating whether to reverse the array.
     * @param forceReverse - An optional boolean flag to force reversing the array.
     * @returns The input array, reversed if shouldReverse or forceReverse is true; otherwise, the input array as is.
     */
    function getReversedArray<T>(input: Array<T>, shouldReverse: boolean, forceReverse?: boolean) {
      if (shouldReverse || forceReverse) {
        return input.reverse();
      }

      return input;
    }

    /**
     * Get Time Series Has Data Changes
     *
     * This function compares two arrays of time series data and returns changes.
     *
     * @param input - The original array of time series data.
     * @param newData - The new array of time series data to compare against.
     * @param shouldReverse - A boolean flag indicating whether to reverse the output array.
     * @returns An array of time series data if changes are detected; otherwise, null.
     */
    function getTimeSeriesHasDataChanges(
      input: Array<vision.DataShapeTimeseriesRow | vision.PIValue>,
      newData: Array<vision.DataShapeTimeseriesRow>,
      shouldReverse: boolean,
    ) {
      // Get the existing timestamps from the symbol.
      const existingTimestampKeys = [
        ...new Set(
          input.flatMap((row) => {
            if ("Values" in row) {
              // Check if row is of type DataShapeTimeseriesRow.
              return row.Values.map((value) => value.Time);
            } else if ("Time" in row) {
              // Check if row is of type PIValue.
              return row.Time;
            } else {
              // Handle other cases or errors accordingly (but this shouldn't occur).
              return [];
            }
          }),
        ),
      ];
      // Get the new time stamps from the data update.
      const newTimestampKeys = [...new Set(newData.flatMap((row) => row.Values.map((value) => value.Time)))];

      // Check for new time series data in the data update.
      const newValues = newData.flatMap((row) =>
        row.Values.filter((value) => existingTimestampKeys.indexOf(value.Time) === -1).map((row) => row.Time),
      );
      // Check for time series data that was in the symbol but was not provided in the data update.
      const removedValues = input
        .flatMap((row) => {
          if ("Values" in row) {
            // Check if row is of type DataShapeTimeseriesRow.
            return row.Values.filter((value) => newTimestampKeys.indexOf(value.Time) === -1);
          } else if ("Time" in row) {
            // Check if row is of type PIValue.
            return newTimestampKeys.indexOf(row.Time) === -1 ? [row] : [];
          } else {
            // Handle other cases or errors accordingly (but this shouldn't occur).
            return [];
          }
        })
        .map((row) => row.Time);

      // Return the array (with reverse based on the current config) only if values have
      // been added or removed. This prevents re-rendering the UI when no changes occur.
      if (newValues.length > 0 || removedValues.length > 0) {
        return newData.map((row) => {
          return {
            ...row,
            Values: getReversedArray(row.Values, shouldReverse),
          };
        });
      }

      return null;
    }

    /**
     * Get Current Locale
     *
     * This function returns the current locale of the browser.
     *
     * @returns The current locale string.
     */
    function getCurrentLocale() {
      return navigator.languages && navigator.languages.length ? navigator.languages[0] : navigator.language;
    }

    /**
     * Format Timestamp
     *
     * This function formats a DateTime object into a localized timestamp string.
     *
     * @param dateTime - The DateTime object to format.
     * @param dateStyle - The style to format the `date` portion of the DateTime with.
     * @param timeStyle - The style to format the `time` portion of the DateTime with.
     * @returns The formatted timestamp string.
     */
    function formatTimestamp(dateTime: Date, dateStyle: DateTimeStyle, timeStyle: DateTimeStyle) {
      if (!dateTime) {
        return dateTime;
      }

      // If the DateTime is invalid and can't be converted, return the original DateTime.
      try {
        const date = new Date(dateTime);
        return new Intl.DateTimeFormat(getCurrentLocale(), { dateStyle, timeStyle }).format(date);
      } catch {
        return dateTime;
      }
    }

    /**
     * Function to allow only numeric input in an input field.
     *
     * @param event - The keyboard event triggered by the user.
     */
    function allowOnlyNumericInput(event: KeyboardEvent) {
      // Allow numeric key codes (0-9) and specific control keys.
      const allowedKeys = ["Backspace", "Tab", "Enter", "Delete", "ArrowLeft", "ArrowRight"];

      if (!allowedKeys.includes(event.key) && (event.key < "0" || event.key > "9")) {
        event.preventDefault();
      }
    }

    return {
      symbolBasePath,
      symbolIconPath,
      configInit,
      getInjectedParams,
      selectSymbolDefaultsFromConfig,
      hasDisplayTimeChanged,
      updateSymbolSingleLabel,
      getSafeColor,
      getHexFromRGB,
      getReversedArray,
      getTimeSeriesHasDataChanges,
      getCurrentLocale,
      formatTimestamp,
      allowOnlyNumericInput,
    };
  })();
  // #endregion
})(window.PIVisualization);
