type SymbolVisualSVG = import("./Symbol/Visual/SVG/declarations").SymbolConfigInit;
type SymbolDataTimeseriesValue = import("./Symbol/Data/TimeseriesValue/declarations").SymbolConfigInit;

declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace vision {
    interface VisualizationExtensibilityEnums {
      DataShapes: vision.ExtensibilityDataShape;
    }

    interface SymbolDefinitionInjectMap {
      mainelyInnovations: InjectMainelyInnovations;
    }

    interface Visualization {
      MainelyInnovations: MainelyInnovations;
      SymMIVisSVG: SymbolVisualSVG;
      SymMIDatTimeseriesValue: SymbolDataTimeseriesValue;
    }

    interface VisionResourceStrings {
      BodyBackgroundColor: "Body BG";
      BodyBorderColor: "Body Border";
      BodyColor: "Body Text";
      BodyTextSize: "Body Text Size";
      BorderColor: "Border";
      BorderRadius: "Border Radius";
      BorderSize: "Border Size";
      ButtonBorderColor: "Button Border";
      ButtonColor: "Button Text";
      ButtonText: "Button Text";
      CheckboxBackgroundColor: "Checkbox BG";
      CheckboxBorderColor: "Checkbox Border";
      CheckboxTickedBackgroundColor: "Checkbox Ticked BG";
      CloseButtonColor: "Close Button Text";
      CloseButtonBackgroundColor: "Close Button BG";
      CloseButtonBorderColor: "Close Button Border";
      CoreOptions: "Core Options";
      DateStyle: "Date Style";
      FooterBackgroundColor: "Footer BG";
      FooterBorderColor: "Footer Border";
      FooterColor: "Footer Text";
      FooterTextSize: "Footer Text Size";
      FormatKeyword: "Format";
      HeaderBackgroundColor: "Header BG";
      HeaderBorderColor: "Header Border";
      HeaderColor: "Header Text";
      HeaderTextSize: "Header Text Size";
      LabelAlignment: "Label Alignment";
      LabelBackgroundColor: "Label BG";
      LabelBorderColor: "Label Border";
      LabelColor: "Label Text";
      LabelTextSize: "Label Text Size";
      InputBackgroundColor: "Input BG";
      InputBorderColor: "Input Border";
      InputColor: "Input Text";
      ModalStyleKeyword: "Modal Style";
      NumberOfRecords: "Number Of Records";
      OpenInNewTab: "Open in new tab";
      SaveButtonColor: "Save Button Text";
      SaveButtonBackgroundColor: "Save Button BG";
      SaveButtonBorderColor: "Save Button Border";
      SelectBackgroundColor: "Select BG";
      SelectBorderColor: "Select Border";
      SelectColor: "Select Text";
      SortIconColor: "Sort Icon";
      TextColor: "Text Color";
      TableHeaderStyleKeyword: "Table Header Style";
      TableRowBorderColor: "Row Border";
      TableRowEvenTextColor: "Even Row Text";
      TableRowEvenBackgroundColor: "Even Row BG";
      TableRowOddTextColor: "Odd Row Text";
      TableRowOddBackgroundColor: "Odd Row BG";
      TableRowStyleKeyword: "Table Row Style";
      TableRowTextSize: "Row Text Size";
      TableColumnSettingsColor: "Column Settings";
      TimeAlignment: "Time Alignment";
      TimeStyle: "Time Style";
      ValueAlignment: "Value Alignment";
      ValueConfigDateTimeStyleShort: "Short";
      ValueConfigDateTimeStyleMedium: "Medium";
      ValueConfigDateTimeStyleLong: "Long";
      ValueConfigDateTimeStyleFull: "Full";
      Vector: "Vector";
      VectorColor: "Vector Color";
    }
  }
}

export {};
