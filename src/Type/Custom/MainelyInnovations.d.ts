declare interface MainelyInnovations {
  symbolBasePath: string;
  symbolIconPath: string;
  configInit: (
    options?: MainelyInnovationsConfigInitOptions,
  ) => <S extends vision.BaseVisionScope<vision.BaseSymbolConfig, vision.BaseSymbolRuntimeData<vision.BaseSymbolConfig>>>(scope: S) => void;
  getInjectedParams: <T extends ReadonlyArray<keyof vision.SymbolDefinitionInjectMap>>(
    inject: T,
    args: vision.SymbolDefinitionArgs<T[number]>,
  ) => vision.InjectedServices<T>;
  selectSymbolDefaultsFromConfig: (allowedKeys: Array<string>) => <C>(config: C) => C;
  hasDisplayTimeChanged: <S extends vision.SymbolScopeHasStartEndTime>(scope: S, timeProvider: vision.InjectTimeProvider) => boolean;
  updateSymbolSingleLabel: <S extends vision.SymbolScopeHasLabel, I extends keyof vision.SymbolDefinitionInjectMap = never>(
    that: vision.SymbolVis<S, I>,
    labelUpdateService: vision.InjectLabelUpdateService,
    callback?: <C = void>(label: string) => C,
  ) => void;
  getSafeColor: (color: string, fallback: string) => string;
  getHexFromRGB: (colorString: string) => string;
  getReversedArray: <T>(input: Array<T>, shouldReverse: boolean, forceReverse?: boolean) => Array<T>;
  getTimeSeriesHasDataChanges: (
    input: Array<vision.DataShapeTimeseriesRow | vision.PIValue>,
    newData: Array<vision.DataShapeTimeseriesRow>,
    shouldReverse: boolean,
  ) => Array<vision.DataShapeTimeseriesRow> | null;
  getCurrentLocale: () => string;
  formatTimestamp: (dateTime: Date | string, dateStyle: DateTimeStyle, timeStyle: DateTimeStyle) => Date | string;
  allowOnlyNumericInput: (event: KeyboardEvent) => void;
}

declare interface MainelyInnovationsConfigInitOptions {
  labelSettingsInit?: boolean;
}

declare interface InjectMainelyInnovations {
  updateSymbolSingleLabel: <S extends vision.SymbolScopeHasLabel, I extends keyof vision.SymbolDefinitionInjectMap = never>(
    that: vision.SymbolVis<S, I>,
    callback?: <C = void>(label: string) => C,
  ) => void;
}

declare type DateTimeStyle = "full" | "long" | "medium" | "short";
