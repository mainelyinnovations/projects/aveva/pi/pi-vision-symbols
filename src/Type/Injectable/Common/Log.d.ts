declare namespace vision {
  interface InjectLog {
    Severity: ExtensibilityLogSeverity;
    LogClearType: ExtensibilityLogClearType;
    ShowInConsole: boolean;
    SeverityOutput: LogClearType;
    shownEntries: number;
    MaxShownEntries: number;
    entries: Array<LogEntry>;
    add: (message: string, severity: LogSeverity, details: LogEntryDetails, clearType: LogClearType) => void;
    remove: (message: string, details: LogEntryDetails) => void;
    getShownEntries: () => Array<LogEntry>;
    dump: (count: number) => void;
    addConnectionError: (details: LogEntryDetails) => void;
    addSearchError: (error: unknown) => void;
    clearEventSearchErrors: () => void;
    clearSearchErrors: () => void;
    clearDataUpdateErrors: () => void;
    clearDisplayErrors: () => void;
    clear: (clearType: LogClearType) => void;
    show: (entry: LogEntry) => boolean;
    generateDownloadLog: () => string;
  }

  interface LogEntry {
    Message: string;
    Severity: LogSeverity;
    Details: LogEntryDetails | string;
    ClearType: LogClearType;
    TimeStamp: Date;
  }

  interface LogEntryDetails {
    status: string;
    statusText: string;
    data: {
      Message: string;
    };
  }
}
