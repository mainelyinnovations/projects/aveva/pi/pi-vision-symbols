declare namespace vision {
  interface InjectTimeZoneSettings {
    decomposeDate: () => TimeZone;
    isCurrent: (tzName: string) => boolean;
    tzClientOverride: string;
    readonly tzString: string;
    tzServerOverride: (tz: string | undefined) => void;
    readonly tzOverride: string;
  }

  interface TimeZone {
    getDatePieces: (date: Date, tz?: string) => TimeZoneDate;
    getName: () => string;
    getTimezoneOffset: (date: Date) => number;
    isDefault: () => boolean;
    valid: () => boolean;
  }

  interface TimeZoneDate {
    month: number;
    day: number;
    year: number;
    hour: number;
    minute: number;
    second: number;
    millisecond: number;
  }
}
