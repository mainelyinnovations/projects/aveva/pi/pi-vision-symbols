declare namespace vision {
  interface InjectAppData {
    readonly zoomLevel: number;
  }
}
