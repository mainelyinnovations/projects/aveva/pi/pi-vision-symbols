declare namespace vision {
  interface InjectPaneSplitters {
    getShellSidebarPaneMin: string;
    getShellSidebarPaneCollapsedSize: string;
    getShellSplitterContainer: () => void;
    getDisplaySplitter: () => kendo.ui.Splitter;
    getShellSplitter: () => kendo.ui.Splitter;
    getEventsSplitter: () => kendo.ui.Splitter;
    getFooterSplitter: () => kendo.ui.Splitter;
    resizeShellContainer: () => void;
    resizeEventDetailsSplitter: () => void;
    hideDisplaySplitter: () => void;
    showDisplaySplitter: () => void;
    hideFooterSplitter: () => void;
    showFooterSplitter: () => void;
    hideShellSplitter: () => void;
    showShellSplitter: () => void;
    hideEventsSplitter: () => void;
    showEventsSplitter: () => void;
    removeDisplayPane: (paneSelector: string) => void;
    removeShellPane: (paneSelector: string) => void;
    collapseDisplayPane: (paneSelector: string, paneIndex: number) => void;
    expandDisplayPane: (paneSelector: string, paneIndex: number) => void;
    collapseShellPane: (paneSelector: string, paneIndex: number) => void;
    expandShellPane: (paneSelector: string, paneIndex: number) => void;
    collapseFooterPane: (paneSelector: string, paneIndex: number) => void;
    expandFooterPane: (paneSelector: string, paneIndex: number) => void;
    collapseEventsPane: (paneSelector: string, paneIndex: number) => void;
    expandEventsPane: (paneSelector: string, paneIndex: number) => void;
    resetSidebarPane: () => void;
  }
}
