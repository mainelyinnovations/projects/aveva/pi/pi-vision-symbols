declare namespace vision {
  interface InjectToolTabsModel {
    TabIndexes: ExtensibilityToolTabIndexes;
    currentTab: number;
    isCollapsed: boolean;
    tabStrip: kendo.ui.TabStrip;
    selectTab: (ecDisplay: boolean) => void;
  }
}
