declare namespace vision {
  interface InjectPIToast {
    SUCCESS: InjectPIToastMode.SUCCESS;
    NEUTRAL: InjectPIToastMode.NEUTRAL;
    ERROR: InjectPIToastMode.ERROR;
    WARNING: InjectPIToastMode.WARNING;
    show: (str: string, options: PIToastOptions) => void;
    success: (str: string, options: PIToastOptions) => void;
    error: (str: string, options: PIToastOptions) => void;
    warn: (str: string, options: PIToastOptions) => void;
    hide: (newToast: JQuery) => void;
    hideAll: () => void;
  }

  interface PIToastOptions {
    backgroundSuccess?: string;
    backgroundNeutral?: string;
    backgroundError?: string;
    backgroundWarning?: string;
    fadeDuration?: number;
    font?: string;
    fontColor?: string;
    fontSize?: string;
    location?: PIToastOptionsLocation;
    minWidth?: string;
    maxWidth?: string;
    mode?: InjectPIToastMode;
    opacity?: number;
    multiplesDirection?: string;
    textShadow?: string;
    waitTime?: number;
  }

  interface PIToastOptionsLocation {
    top: string;
    left: string;
  }
}
