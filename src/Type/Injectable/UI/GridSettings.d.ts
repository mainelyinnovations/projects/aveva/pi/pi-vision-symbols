declare namespace vision {
  interface InjectGridSettings {
    readonly defaultGridSize: number;
    readonly displayHasGridDots: boolean;
    readonly gridDotSpacingFactor: number;
    gridDotSpacingFactorInterval: number;
    readonly numberOfGridDotSpacingFactorIntervals: number;
    showGridDots: boolean;
    snapToGrid: boolean;
  }
}
