declare namespace vision {
  interface InjectAssetContext {
    readonly triggerSwapAsset: SignalHelper;
    readonly assetSelectMenuEnabled: boolean;
    readonly assetsOnDisplay: Array<string>;
    readonly contextSearchResults: Array<string>;
    readonly relatedAssetsForSelectedAsset: Array<string>;
    readonly pendingRequest: Promise<unknown>;
    selectedAsset: string;
    swappedAssets: { [key: string]: unknown };
    displayAssetsMapping: { [key: string]: string };
    init: () => void;
    getDisplayedAssets: (refresh: boolean, filterAllDuplicates: boolean) => void;
    getRelatedAssets: (performAssetSearch: boolean) => Promise<void>;
    swapAssets: (path: string) => void;
    getSwappedAssetPaths: () => Array<string>;
    onDisplayChange: (swapperSearchConfigChanged: boolean) => void;
    refreshAssetSwapperContextName: typeof ng.noop;
    cancelRelatedAssetsRequest: () => void;
    clearAssetsOnDisplay: () => void;
  }
}
