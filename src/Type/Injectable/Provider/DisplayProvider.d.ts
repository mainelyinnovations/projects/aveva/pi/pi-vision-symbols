declare namespace vision {
  interface InjectDisplayProvider {
    readonly displayPath: string;
    readonly drawingModeName: string;
    drawingMode: (val: unknown) => unknown;
    readonly containsSymbolsWithDataSources: boolean;
    displayId: number;
    displayName: string;
    instance: {
      DisplayProperties: DisplayProperties;
      Symbols: Array<SymbolVis<BaseVisionScope<BaseSymbolConfig>>>;
    };
    requestId: string;
    readOnly: boolean;
    hasDisplayEditAccess: boolean;
    revision: number;
    layoutMode: boolean;
    kioskMode: boolean;
    defaultSymbol: string;
    defaultCollectionSymbol: unknown;
    forceUpdate: boolean;
    canSave: boolean;
    canSaveAs: boolean;
    isDirtyDisplay: boolean;
    eventFrameSearch: { instance: DisplayPropertiesEventFrameModel };
    isEventSearchChanged: boolean;
    pinnedEventsRequests: Array<{ Id: string; Path: string }>;
    BackgroundColor: string;
    GridSize: number;
    displayAreaHeight: number | undefined;
    refreshDisplayDimensions: () => void;
    readonly displayThemeName: string | undefined;
    displayCalculations: Array<DisplayCalculation>;
    FitAll: boolean;
    showAssetPaths: boolean;
    contextSwapSource: string;
    contextSwapAction: string | null;
    contextSwapSearchCriteria: unknown | undefined;
    readonly symbolGroupParents: unknown;
    displayEventsAutoRefresh: boolean;
    readonly eventFrameSearchConfigured: boolean;
    readonly pushUndoChange: SignalHelper;
    init: () => void;
    load: (display: Display) => void;
    loadDisplay: (
      dispId: number,
      complete: typeof ng.noop,
      error: typeof ng.noop,
      useCache?: boolean,
      preserveRuntimeEditorState?: boolean,
      forceReloadDisplayCallback?: typeof ng.noop,
    ) => void;
    alignSelectedSymbols: (positionType: string) => unknown;
    deselectGroupChildren: (parent: Array<SymbolVis<BaseVisionScope<BaseSymbolConfig>>>) => void;
    distributeSelectedSymbols: (distributeType: string) => void;
    applyDisplayState: (display: Display, refreshSymbolRuntimes: boolean) => void;
    applyDisplayEventFrameSearch: (criteria: unknown, changed: boolean) => void;
    applyData: (data: unknown) => void;
    addSymbol: (
      symbolOrTypeName: string,
      x: number,
      y: number,
      dataSources: Array<string>,
      config: BaseSymbolConfig,
    ) => SymbolVis<BaseVisionScope<BaseSymbolConfig>>;
    applyDisplayThemeToSymbol: (symbol: SymbolVis<BaseVisionScope<BaseSymbolConfig>>, themeName: string) => void;
    canAdHoc: (singleSymbol?: SymbolVis<BaseVisionScope<BaseSymbolConfig>>) => boolean;
    getTempShape: (x: number, y: number, preventDraw: boolean) => SymbolVis<BaseVisionScope<BaseSymbolConfig>>;
    createNewDisplay: (complete: typeof ng.noop) => void;
    configAdHocSymbol: (symbol: SymbolVis<BaseVisionScope<BaseSymbolConfig>>, symbolType: string) => SymbolVis<BaseVisionScope<BaseSymbolConfig>>;
    convertAdHocDisplay: (complete: typeof ng.noop) => void;
    addChildrenToSymbolGroupParents: (children: Array<string>, parent: string) => void;
    removeChildrenFromSymbolGroupParents: (children: Array<string>) => void;
    findSymbolParent: (symbolName: string) => string;
    generateServerDisplay: (clientDisplay: DisplayState | null, nameOverride: string, noTimeInfo: boolean, idOverride: boolean) => DisplayState;
    getSymbolByName: (name: string, isCollectionMember?: boolean) => SymbolVis<BaseVisionScope<BaseSymbolConfig>>;
    getRuntimeSymbolData: (name: string) => SymbolVis<BaseVisionScope<BaseSymbolConfig>>;
    getSymbolPosition: (symOrName: SymbolVis<BaseVisionScope<BaseSymbolConfig>> | string) => unknown | null;
    getRuntimeDataArray: (symbols?: Array<SymbolVis<BaseVisionScope<BaseSymbolConfig>>>) => Array<SymbolVis<BaseVisionScope<BaseSymbolConfig>>>;
    getDrawingOption: (optionName: string) => unknown;
    groupSelectedSymbols: () => void;
    isGroup: (newType: string) => boolean;
    isCollection: (newType: string) => boolean;
    cloneSymbol: (origSymbol: SymbolVis<BaseVisionScope<BaseSymbolConfig>>) => SymbolVis<BaseVisionScope<BaseSymbolConfig>>;
    removeSymbol: (name: string) => void;
    getLastSelectedSymbol: () => SymbolVis<BaseVisionScope<BaseSymbolConfig>> | null;
    getSelectedSymbols: (
      asObject: boolean,
      filterGroups?: boolean,
      omitGroupChildren?: boolean,
      inZOrder?: boolean,
    ) => Array<SymbolVis<BaseVisionScope<BaseSymbolConfig>>>;
    selectAll: () => void;
    isChildSelected: (parentName: string) => boolean;
    selectGroupChildren: (parent: SymbolVis<BaseVisionScope<BaseSymbolConfig>>) => void;
    toggleSelectionType: () => void;
    selectSymbol: (selectedSymName?: SymbolVis<BaseVisionScope<BaseSymbolConfig>> | string, touchUsed?: boolean, ctrlPressed?: boolean) => void;
    selectMultipleSymbols: (newSymbolName: string, touchUsed: boolean) => void;
    setDisplayPath: (path: string) => void;
    switchSymbolToCollection: (single: boolean, switchComplete: typeof ng.noop) => void;
    switchSymbolToType: (context: unknown, newType: string) => void;
    toggleSymbolSelection: (symbolName: string) => void;
    moveSymbolsCompleted: () => void;
    moveSelectedSymbolsForward: () => void;
    moveSelectedSymbolsBackward: () => void;
    moveSelectedSymbolsToFront: () => void;
    moveSelectedSymbolsToBack: () => void;
    canMoveFront: () => boolean;
    canMoveBack: () => boolean;
    canAlign: () => boolean;
    canDistribute: () => boolean;
    createAdHocDisplay: (
      symbolType: string,
      dataSources: Array<Datasource>,
      calculations: Array<DisplayCalculation>,
      complete: typeof ng.noop,
    ) => void;
    resolveItemsWithDatasources: (items: Array<unknown>, callback: typeof ng.noop) => void;
    addAttachment: () => unknown;
    getAttachment: (id: unknown, localOnly: unknown) => unknown;
    getAttachmentsForSave: (saveAll: boolean) => Array<unknown>;
    displaySaved: (data: unknown) => void;
    boundingBoxCoordinatesForSymbols: (selectedSymbolsRuntimeArray: Array<SymbolVis<BaseVisionScope<BaseSymbolConfig>>>) => {
      topMostCornerPoint: number;
      leftMostCornerPoint: number;
      rightMostCornerPoint: number;
      bottomMostCornerPoint: number;
    };
    selectionChangedSignal: SignalHelper;
    symbolHasDatasource: (symbol: SymbolVis<BaseVisionScope<BaseSymbolConfig>>, checkDynamicDataForAdhoc?: boolean) => boolean;
    generateECServerDisplay: (datasources: Array<string>, nameOverride?: string) => unknown;
    calcSymbolSize: (symbolType: string, datasources: Array<string>, config: BaseSymbolConfig) => void;
    symbolsAllowCursors: () => string;
    symbolsWithDefaultTimeRange: () => string;
    hasTrends: () => boolean;
    isSymbolExpandable: (symName: string) => boolean;
    symbolIsLink: (symName: string) => boolean;
    addPinnedEventRequest: (ef: EventComparisonPinnedEvent) => void;
    removePinnedEventRequest: (ef: EventComparisonPinnedEvent) => void;
    resetPinnedEventsRequests: () => void;
    updatePinnedEventsRequests: (events: Array<EventComparisonPinnedEvent>) => void;
    enterCollectionEditMode: (symbol: SymbolVis<BaseVisionScope<BaseSymbolConfig>>) => void;
    exitCollectionEditMode: () => void;
    isCollectionInEditMode: () => boolean;
    isThisCollectionInEditMode: (symbol: SymbolVis<BaseVisionScope<BaseSymbolConfig>>) => boolean;
    notifyCollectionOnUndoRedo: () => void;
    requestUpdate: (dataUpdate: boolean, changedSymbolsOnly: boolean) => void;
    isRequestDataUpdateRequire: () => {
      dataUpdate: boolean;
      changedSymbolsOnly: boolean;
    };
    setCollectionParams: (sym: SymbolVisCollection<BaseVisionScope<BaseSymbolConfig>>, callback: typeof ng.noop) => void;
    getSymbolsFromCollection: (
      collectionSym: SymbolVisCollection<BaseVisionScope<BaseSymbolConfig>>,
    ) => Array<SymbolVis<BaseVisionScope<BaseSymbolConfig>>>;
    displayContentModified: () => boolean;
    addDisplayCalculation: (val: DisplayCalculation) => void;
    getCalculationFromPath: (path: string) => DisplayCalculation | undefined;
    displayAFAssetsAndAttributes: () => Array<string>;
    getIndexForSymbolName: (name: string) => number;
    getDistinctAssetsFromSymbolDatasources: (parameters: GetDistinctAssetsParameters) => Array<string>;
    displayHasDynamicDatasources: () => boolean;
    allCustomDataRequestsCompleted: () => ng.IQService["resolve"] | ng.IQService["all"];
    getDefaultDisplayProperties: () => DisplayDefaults & DisplayDefaultsCustom;
    selectDisplayDefaultsFromProperties: (props: object) => object;
  }

  // applyDisplayEventFrameSearch
  // EventSearchFilterForSave

  interface GetDistinctAssetsParameters {
    includeDynamicDatasources?: boolean;
    ignoreLinksToThisDisplay?: boolean;
    ignoreStaticLinksToOtherDisplays?: boolean;
    includeAFAttributes?: boolean;
    filterAllDuplicates?: boolean;
  }
}
