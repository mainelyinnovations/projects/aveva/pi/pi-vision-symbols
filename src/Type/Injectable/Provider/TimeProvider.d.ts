declare namespace vision {
  interface InjectTimeProvider {
    displayTime: TimeProviderDisplayTime;
    timeRangeEvent: TimeProviderTimeRangeEvent;
    model: unknown;
    getServerStartTime: () => string;
    getServerEndTime: () => string;
    getServerIsUpdating: () => boolean;
    isEndTimeChanged: () => boolean;
    create: () => unknown;
    processServerResponse: (responseData: unknown) => void;
    notifyDisplayTimeUpdated: () => void;
    notifyOnNextUpdate: (timeChanged: boolean, assetChanged: boolean) => void;
    requestNewTime: (startParam: string, endParam: string, notify: boolean, isNewEndTime: boolean) => void;
    applyNewDisplayTimeObject: (newDisplayTime: string, notify: boolean) => void;
    applyNewTimeRangeEventPath: (path: string) => void;
    applyNewTimeRangeEventObject: (newEvent: string) => void;
    panTimeRange: (percent: number) => void;
    scaleTimeRange: (scale: number, centerPrec: number) => void;
    _calculateGestureTime: () => void;
    setZoomedTimeRange: (startPercent: number, endPercent: number) => void;
    gestureComplete: (applyTime: boolean) => void;
    gestureOffsetAndScale: () => TimeProviderGesture;
    resetTimeRangeEventPath: () => void;
    getDisplayStartTime: () => string;
    getDisplayEndTime: () => string;
  }

  interface TimeProviderDisplayTime {
    start: string;
    end: string;
  }

  interface TimeProviderTimeRangeEvent {
    path: string | null;
  }

  interface TimeProviderGesture {
    offsetPercent: number;
    scale: number;
  }
}
