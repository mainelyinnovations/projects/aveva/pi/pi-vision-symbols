declare namespace vision {
  interface InjectDataPump {
    isRunning: boolean;
    timeoutPromise: ng.ITimeoutService;
    requestOnlyChangedSymbolsOnce: boolean | null;
    selectedSymbolName: string | null;
    lastCallIdReceived: number;
    readonly timeoutCompleted: boolean;
    readonly updateFrequency: number;
    readonly ext: DataPumpExt;
    init: (requestDataUpdate: typeof ng.noop, extendObject: DataPumpExt) => void;
    requestUpdate: (changedSymbolsOnly: boolean) => void;
    requestUpdateForSelectedSymbol: (name: string) => void;
    scheduleUpdate: (requestTime: number) => void;
    start: () => void;
    stop: () => void;
  }

  interface DataPumpExt {
    requestDataUpdate: typeof ng.noop;
  }
}
