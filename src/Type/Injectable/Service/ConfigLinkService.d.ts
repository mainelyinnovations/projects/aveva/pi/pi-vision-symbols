declare namespace vision {
  interface InjectConfigLinkService {
    readonly symbolIsStatic: boolean | undefined;
    readonly isBusy: boolean;
    readonly isValidLinkURL: boolean;
    readonly isValidAssetPath: boolean;
    readonly searchResults: Array<DisplayInfo> | undefined;
    readonly dropAccept: string;
    target: string;
    linkURL: string;
    includeTimeRange: boolean;
    includeAsset: boolean;
    assetOptionsEnabled: boolean;
    linkAsset: string;
    newTab: boolean;
    onShowConfigPane: () => void;
    deleteLink: () => void;
    searchDisplays: (search: string) => void;
    setDisplayLink: (result: ConfigLinkServiceDisplayLinkResult) => void;
    isPBDisplayLink: (link: string) => boolean;
    launchLink: (url: string, newTab: boolean) => void;
    dragOver: (event: DragEvent, ui) => void;
    dragOut: (event: DragEvent, ui) => void;
    dropOn: (event: DragEvent, ui) => void;
  }

  interface ConfigLinkServiceDisplayLinkResult {
    DisplayLink: string;
  }
}
