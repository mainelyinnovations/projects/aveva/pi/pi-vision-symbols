declare namespace vision {
  interface InjectLabelUpdateService {
    init: <C, I extends keyof SymbolDefinitionInjectMap = never>(visObj: SymbolVis<C, I>, callback: (label: string) => void) => void;
  }

  interface VisualizationUpdateService {
    getLabel: (config: any, options: VisualizationUpdateServiceGetLabelOptions) => string;
  }

  interface VisualizationUpdateServiceGetLabelOptions {
    Label: string;
    Description: string;
  }
}
