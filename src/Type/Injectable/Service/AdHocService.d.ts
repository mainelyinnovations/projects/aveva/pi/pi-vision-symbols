declare namespace vision {
  interface InjectAdHocService {
    adHocActive: boolean;
    readonly adHocExists: boolean;
    readonly newDatasourcesWhileClosed: number;
    launchAdHocTrend: (command: AdHocServiceCommand) => void;
    addDatasourceToAdHoc: (symbolList: BaseSymbolList, datasources: Array<string>, attributeFilter: string | undefined) => void;
  }

  interface AdHocServiceCommand {
    newAdHoc: boolean;
    newPopup: boolean;
    symbolList: BaseSymbolList;
  }
}
