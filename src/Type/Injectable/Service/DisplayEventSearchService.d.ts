declare namespace vision {
  interface InjectDisplayEventSearchService {
    readonly eventsNumber: number;
    readonly attributeFilterEnabled: boolean;
    refreshEventList: boolean;
    undoRedoChangedEventSearch: boolean;
    searchModel: EventSearchModel;
    preloadedModel: EventSearchPreloadedModel;
    timeoutPromise: ng.ITimeoutService | null;
    timeoutNoResponsePromise: ng.ITimeoutService | null;
    forceRefresh: boolean;
    pendingCalls: { [key: string]: Promise<unknown> };
    getTopLevelCallback: boolean | null;
    getEventFrameAttributeTemplatesCallback: boolean | null;
    eventFrameUpdateFrequency: number;
    addAttrValueFilter: () => void;
    apply: (model: DisplayPropertiesEventFrameModel) => void;
    applyEventSearch: () => void;
    assetsOnDisplayOptionHidden: () => boolean;
    attrTemplateChanged: (index: number, operator: string | undefined, value: string | undefined) => void;
    attrValueDropdownShown: (index: number) => boolean;
    attrValueInputBoxShown: (index: number) => boolean;
    isAttrValueNumeric: (valDataType: string) => boolean;
    cancelAll: () => void;
    cancelRequest: () => void;
    canNotApply: () => boolean;
    createSearchModel: () => EventSearchModel;
    databaseChanged: (elementTemplate: string, categoryName: string, eventFrameTemplate: string, attributeValues: Array<AttributeValue>) => void;
    durationHandler: () => void;
    eventFrameTemplateChanged: (attributeValues: Array<AttributeValue>) => void;
    loadEventSearchCriteria: (customSearchModel?: DisplayPropertiesEventFrameModel) => void;
    numberOfResultsHandler: () => void;
    refreshChange: (forceUpdate?: boolean) => void;
    refreshChangeCheckBoxClicked: () => void;
    removeAttrValueFilter: (index: number) => void;
    resetEventSearch: (clear?: boolean) => void;
    revertSearchModel: () => void;
    searchEvents: (interrupt: boolean, isNewSearch?: boolean, loadCompleteCallback?: typeof ng.noop) => void;
    selectDatabaseInfo: (
      dbName: string | null,
      elementTemplate: string,
      categoryName: string,
      templateID: string,
      attributeValues: Array<AttributeValue>,
      loadDatabasesOnly: boolean,
    ) => void;
    selectOptions: (elementTemplate, categoryName: string, templateID: string, attributeValues: Array<AttributeValue>) => void;
    timeRangeOptionShown: (option: TimeRangeOption) => boolean;
    updateSelectedTimeRangeOption: () => void;
    buildSearchQuery: (
      eventtype: string,
      eventname: string | null,
      fullElementPath: string,
      starttime: string,
      alldescendants: boolean,
      eventTypeId: string | null,
      numberofResults: number,
      endtime: string,
      assetName: string,
      sortOrder: boolean,
    ) => void;
    loadNewCriteria: () => void;
    getSearchFilter: () => EventFrameSearchFilter;
    buildSearchFilter: (model: DisplayPropertiesEventFrameModel, custom?: DisplayPropertiesEventFrameModel) => EventFrameSearchFilter;
    cancelPendingCalls: () => void;
    getMaxCount: () => number | null;
    createDisplayAgnosticSearchCriteria: () => DisplayPropertiesEventFrameModel;
    scheduleUpdate: (isNewSearch?: boolean) => void;
    isDurationValid: () => boolean;
  }

  interface EventSearchPreloadedModel {
    databaseList: DatabaseCollection | null;
    eventSeverityOptions: Array<EventSeverityOption>;
    timeRangeOptions: Array<TimeRangeOption>;
    durationUnitOptions: Array<DurationUnitOption>;
    eventAttrOperators: {
      Equal: AttributeOperator;
      GreaterThan: AttributeOperator;
      GreaterThanOrEqual: AttributeOperator;
      In: AttributeOperator;
      LessThan: AttributeOperator;
      LessThanOrEqual: AttributeOperator;
      NotEqual: AttributeOperator;
    };
    eventAttrDataTypes: {
      Bool: AttributeDataType;
      Decimal: AttributeDataType;
      Default: AttributeDataType;
      Digital: AttributeDataType;
      Float: AttributeDataType;
      Guid: AttributeDataType;
      Integer: AttributeDataType;
      String: AttributeDataType;
      Time: AttributeDataType;
    };
    readyToShow: boolean;
    formatWithSubSeconds: string;
  }

  interface EventSearchModel {
    readonly selectedAssetNameOption: number;
    readonly selectedTimeRangeOption: TimeRangeOption;
    errorMessage: string | null;
    init: boolean;
    selectedEventName: null;
    selectedStartTime: string;
    selectedEndTime: string;
    selectedElementNames: null;
    selectedComments: string;
    selectedAcknowledgement: string;
    selectedEventState: string;
    selectedNumberOfResultsOption: string;
    selectedNumberOfResultsAscendingLimit: number;
    selectedNumberOfResultsDescendingLimit: number;
    selectedNumberOfResultsAscending: boolean;
    selectedNumberOfResultsLabel: string;
    selectedNumberOfResultsAll: boolean;
    selectedDurationOption: string;
    selectedMinDuration: number;
    selectedMaxDuration: number;
    selectedDurationLabel: string;
    selectedDurationAny: boolean;
    selectedMinDurationUnitOption: DurationUnitOption;
    selectedMaxDurationUnitOption: DurationUnitOption;
    selectedEventTypeLabel: string;
    selectedSearchMode: string;
    selectedAttrValues: { [key: number]: AttributeValue };
    selectedEventSeverityOption: null;
    selectedAssetTypeOption: null;
    selectedEventCategoryOption: null;
    selectedAllDescendants: boolean;
    readyToShow: boolean;
  }

  interface EventSeverityOption {
    $$hashKey: string;
    name: string;
    label: string;
    value: string;
  }

  interface TimeRangeOption {
    $$hashKey: string;
    startTime: string;
    endTime: string;
    label: string;
    value: string;
    getStartTime: () => string;
    getEndTime: () => string;
  }

  interface DurationUnitOption {
    $$hashKey: string;
    name: string;
    value: number;
  }

  interface AttributeDataType {
    operators: Array<AttributeOperator>;
  }

  interface AttributeOperator {
    symbol: string;
    value: string;
  }

  interface AttributeValue {
    Name: string;
    Value: string;
    Operator: AttributeOperator;
  }
}
