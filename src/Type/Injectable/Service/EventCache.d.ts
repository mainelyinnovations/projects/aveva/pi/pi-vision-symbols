declare namespace vision {
  interface InjectEventCache {
    readonly displayedEventsPaths: Array<string>;
    readonly eventsNumber: number;
    readonly eventsRuntime: EventsRuntime;
    cancelAll: () => void;
    cancelRelatedEventFrames: () => void;
    emptyElement: () => void; //
    getPinnedChildren: (parent: string, allowMultipleRequests?: boolean) => Promise<unknown>;
    getUnPinnedChildren: (parent: string, allowMultipleRequests?: boolean) => Promise<unknown>;
    getECNew: (item: EventFrame, $state: unknown, compareByName: boolean) => void;
    init: () => void;
    isCached: (parent: string) => boolean;
    isCachedPinned: (parent: string) => boolean;
    refresh: (eventsCollection: Array<EventFrame>) => void;
    searchEvents: (
      requestId: string,
      searchFilter: EventFrameSearchFilter,
      isAssetsOnDisplay: boolean,
      timeRange: TimeRange,
      maxCount: number,
      isNewSearch: boolean,
    ) => Promise<unknown>;
    pinEvent: (ef: EventFrame) => void;
    unpinEvent: (ef: EventFrame) => void;
  }

  interface EventsRuntime {
    _palCount: number;
    _markerCount: number;
    palIndexPinned: number;
    palLRU: Array<number>;
    markerLRU: Array<number>;
    eventStates: Array<EventFrameState>;
    childAlignment: string;
    childAlignmentDetails: string;
    highlightedPath: string;
    isZoomed: boolean;
  }
}
