declare namespace vision {
  interface InjectAppClipboard {
    ClipboardChanged: string;
    clear: () => void;
    getSymbols: () => AppClipboardRetrieval;
    setSymbols: (symbols: BaseSymbolList, attachments: Array<DisplayAttachment>, requestId: string, calculations: Array<DisplayCalculation>) => void;
  }

  interface AppClipboardRetrieval {
    symbols: BaseSymbolList;
    attachments: Array<DisplayAttachment>;
    requestId: string;
    calculations: Array<DisplayCalculation>;
  }
}
