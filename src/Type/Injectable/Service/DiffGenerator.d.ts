declare namespace vision {
  interface InjectDiffGenerator {
    readonly differences: Array<unknown>;
    readonly symbolAddedOrRemoved: boolean;
    readonly reorderOnly: boolean;
    dataSourcesChanged: boolean;
    init: () => void;
    generateRenameDiff: (prevDisplayState: DisplayState, currDisplayState: DisplayState) => void;
    generateAndAddDiffIfNecessary: (previousState: DisplayState, currentState: DisplayState) => void;
  }

  interface SymbolDiff {
    ChangeType: number;
    Index: number;
    Symbol?: SymbolScopeBase;
  }
}
