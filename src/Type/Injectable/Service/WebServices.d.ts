declare namespace vision {
  interface InjectWebServices {
    cancelAllPendingRequest: () => void;
    acknowledgeEventFrame: (server: string, eventFrameId: string) => unknown;
    annotateEventFrame: (server: string, eventFrameId: string, comment: string, attachmentName: string, attachmentData: string) => unknown;
    deleteDisplay: (displayId: number) => unknown;
    getFileAttachmentDefaults: (serverName: string) => unknown;
    getAttachment: (path: string, annotationId: string) => unknown;
    getAllLabels: () => unknown;
    getAllUsers: () => unknown;
    postMatchRelatedAssets: (displayPaths: Array<string>, newPaths: Array<string>) => unknown;
    getChildAssets: (parentPath: string, uniqueId: string) => unknown;
    getChildAttributes: (parentPath: string, uniqueId: string) => unknown;
    getChildFolders: (displayFolderId: number) => unknown;
    getHierarchy: (displayFolderId: number) => unknown;
    createNewDisplayFolder: (parentFolderId: number, folderName: string) => unknown;
    deleteVisionFolder: (folderId: number, confirmDelete: boolean) => unknown;
    getFolderPermissions: (id: number) => unknown;
    putFolderSettings: (folderId: number, identities: Array<unknown>, newName: string, optionalPropagate: unknown) => unknown;
    getDatabases: () => unknown;
    getDefaultStates: (path: string, requireDefaultStates: boolean, overrideUOM: boolean) => unknown;
    postForDatasourceMetadata: (datasources: Array<string>) => Array<DatasourceMetadata>;
    postForRelatedAttributeMetadataForAssets: (assetsAndAttributes: WebServicesAssetsAndAttributes) => unknown;
    postForUOMsInSameClasses: (afServer: string, uoms: Array<unknown>) => unknown;
    getDisplayForEditing: (displayId: number, noCache: boolean) => unknown;
    getAllUOMs: (afServer: string) => unknown;
    convertValuesToNewUOM: (afServer: string, sourceUOM: unknown, destUOM: unknown, values: Array<unknown>) => unknown;
    getDisplayInfo: (displayId: number) => unknown;
    getDisplayLabelsWithIds: (displayId: number) => unknown;
    getDisplayList: (params: unknown) => unknown;
    getFolderContents: (params: unknown) => unknown;
    getEventChildAttributes: (path: string, eventFrameId: string, parentId: string) => unknown;
    getEventComparisonDisplay: (displayId: number) => unknown;
    getEventFrameAttributeEnumValues: (serverDatabaseName: string, eventTemplateId: string, attributeTemplateId: string) => unknown;
    getEventFrameChildren: (eventFrameId: string, path: string) => unknown;
    getEventFrameDetails: (server: string, eventFrameId: string) => unknown;
    getEventTopLevelAttributes: (path: string, eventFrameId: string) => unknown;
    getEventDisplayAttributes: (requestId: string, eventFrameId: string, refElemPath: string) => unknown;
    getEventCategoriesAndTemplates: (serverDatabaseName: string, includeEFTemplates: boolean) => unknown;
    getEventFrameAttributeTemplates: (serverDatabaseName: string, eventFrameTemplateId: string, isElement: boolean) => unknown;
    getLabelsRanks: (params: object) => unknown;
    getNewDisplay: () => unknown;
    getPlottableAttributesPath: (parentPath: string) => unknown;
    getReferencedElementChildAttributes: (path: string, eventFrameId: string, parentId: string) => unknown;
    postAssetsWithTemplate: (paths: Array<string>) => unknown;
    postRelatedAssets: (assetPath: string, unresolvedDatasources: Array<string>) => unknown;
    postDisplayContextAssetSearch: (searchModel: unknown, unresolvedDatasources: Array<string>) => unknown;
    getRelatedEventFrames: (
      requestId: string,
      elementPaths: string,
      startTime: string,
      endTime: string,
      maxCount?: number,
      searchFilter?: AssetSearchFilter,
      pinnedEvents?: Array<unknown>,
    ) => unknown;
    getEventsTableRelatedEventFrames: (requestId: string, elementPaths: string, searchFilter: AssetSearchFilter, config: BaseSymbolConfig) => unknown;
    getEventFrameReasons: (server: string, eventFrameId: string) => unknown;
    postEventFrameReason: (server: string, eventFrameId: string, reason: string) => unknown;
    getSearchResults: (searchPattern: string, path: string | null, checkedSources: Array<string> | null, filters: Array<SearchFilter>) => unknown;
    getRootName: (assetPath: string) => unknown;
    setDisplayDefaults: (name: string, value: string, revision: number) => unknown;
    getAssetTemplate: (assetPath: string) => unknown;
    validateTime: (time: string, isOffset: boolean) => unknown;
    normalizeXYTime: (
      startTime: string,
      endTime: string,
      startOffset: boolean,
      endOffset: boolean,
      displayStartTime: string,
      displayEndTime: string,
    ) => unknown;
    previewCalculation: (calculation: DisplayCalculation) => unknown;
    getTimeoutInfo: () => unknown;
    getGraphicLibraryCategories: () => unknown;
    getLibraryGraphics: (directoryKey: string) => unknown;
    loadGraphic: (directoryKey: string, fileKey: string) => unknown;
    openAttachment: (requestId: string, id: string) => unknown;
    postDiffForData: (changeRequest: DiffForDataRequest, requestId: string) => unknown;
    postDownloadExportData: (displayRequest: DisplayRequest, requestId: string, fileType: string) => unknown;
    downloadExportData: (requestId: string, starttime: string, endtime: string, fileType: string) => unknown;
    getDownloadExportData: (requestId: string) => unknown;
    // download?
    postDisplayForData: (
      displayDefinition: DiffForDataRequest,
      requestId: string,
      starttime: string,
      endtime: string,
      symbols: BaseSymbolList,
      cursorTimes: Array<string>,
      eventFramePath: string,
      currentCollectionName: string,
    ) => unknown;
    postDisplayWithNoData: (displayDefinition: DiffForDataRequest, requestId: string) => unknown;
    postTagEndOfStreamValues: (piServer: string, tags: Array<string>) => unknown;
    postMoveDisplays: (request: MoveDisplayRequest) => Array<unknown>;
    postOverlayTrend: (query: OverlayTrendQuery, requestId: string) => unknown;
    postRefreshEvents: (requests: unknown) => unknown;
    postSaveDisplay: (displayRequest: SaveDisplayRequest) => unknown;
    postSaveEventComparisonDisplay: (request: SaveEventComparisonRequest) => unknown;
    postSaveDisplayByRequestId: (requestId: string, changeRequest: SaveDisplayRequestById) => unknown;
    validateRevision: (id: string, revision: number) => unknown;
    postEventDetailsData: (
      requestId: string,
      symbols: BaseSymbolList,
      eventFramePath: string,
      zeroOffset: number,
      cursorTimes: Array<string>,
      startTime: string,
      endTime: string,
    ) => unknown;
    putDisplayFavorite: (displayId: number, isFavorite: boolean) => unknown;
    putDisplaySettings: (
      displayId: number,
      identities: Array<VisionIdentity>,
      isReadOnly: boolean,
      labels: Array<string>,
      owner: string,
      showPrivateDisplays: boolean,
    ) => unknown;
    putUserSettingSearchExcludedServers: (value: string) => unknown;
    windowsAuthSignIn: () => void;
  }

  interface WebServicesAssetsAndAttributes {
    Assets: Array<unknown>;
    Attributes: Array<unknown>;
  }

  interface SaveDisplayRequest {
    StartTime: string;
    EndTime: string;
    EventFramePath: string;
    Display: SaveDisplay;
    Attachments: Array<DisplayAttachment>;
    FolderId: number | null;
  }

  interface SaveDisplayRequestById {
    StartTime: string;
    EndTime: string;
    EventFramePath: string;
    Changes: { [key: number]: Array<SymbolDiff> };
    Attachments: Array<DisplayAttachment>;
  }

  interface SaveEventComparisonRequest {
    Display: DisplayBase;
    FolderId: number | null;
  }

  interface SaveDisplay extends DisplayBase {
    Symbols: BaseSymbolList;
  }

  interface EventComparisonPinnedEvent {
    Id: number;
    Path: string;
  }

  interface DiffForDataRequest {
    Changes: { [key: number]: Array<SymbolDiff> };
    EventFramePath?: string;
    StartTime?: string;
    EndTime?: string;
    IncludeMetadata: boolean;
    CurrentCollectionName?: string;
    ForceUpdate?: boolean;
    CursorTimes?: Array<string>;
    Symbols?: Array<unknown>;
    SymbolIncludeLimits?: string;
  }

  interface DisplayRequest {
    StartTime: string;
    EndTime: string;
    EventFramePath: string;
    Display: string;
    Attachments: string;
  }

  interface MoveDisplayRequest {
    DisplayIds: Array<number>;
    FolderId: number;
  }

  interface OverlayTrendQuery {
    EventNames: Array<string>;
    Events: Array<string>;
    RefElementPaths: Array<string>;
    Traces: Array<string>;
    TrendSettings: unknown;
    Assets: Array<string>;
    Paths: Array<string>;
    RootCause: number;
    Intervals: number;
    Masks?: Array<boolean> | null;
    RootCauseList?: Array<boolean> | null;
    Requests?: Array<OverlayTrendQueryRequest>;
    AlignChild?: string;
    AlignChildDetails?: string;
    Zoom?: boolean;
    CursorOffsets?: Array<number> | boolean | null;
    AdHoc?: OverlayTrendQueryAdHoc | null;
  }

  interface OverlayTrendQueryAdHoc {
    ZeroOffset: number;
    Duration: number;
  }

  interface OverlayTrendQueryRequest {
    Id: string;
    Path: string;
    EventName: string;
    GetChildren: boolean;
    TopLevel: boolean;
  }
}
