declare namespace vision {
  interface InjectReloadDisplayService {
    shouldStoreDisplay: () => boolean;
    storeDisplay: (displayToSave: unknown) => void;
    retrieveDisplay: (displayId: number) => unknown;
    restoreDisplayHeaderState: (display: ReloadDisplayServiceDisplay) => void;
    saveEditorDisplay: (undoStack: Array<unknown>) => void;
    restoreEditorDisplayState: (display: ReloadDisplayServiceDisplay, undoStack: Array<unknown>) => void;
    saveProperty: (name: string, getter: unknown, setter: unknown) => void;
  }

  interface ReloadDisplayServiceDisplay {
    StartTime: string;
    EndTime: string;
    EventFramePath: string;
    Attachments: Array<ReloadDisplayServiceDisplayAttachment>;
    newImages: Array<unknown>;
    undoStack: Array<unknown>;
  }

  interface ReloadDisplayServiceDisplayAttachment {
    m_Item1: string;
    m_Item2: string;
  }
}
