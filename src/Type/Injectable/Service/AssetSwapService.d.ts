declare namespace vision {
  interface InjectAssetSwapService {
    swapDisplayTemplatedAssets: (oldAsset: string, newAsset: string, symbolTriggeringSwap: string) => void;
    replaceDatasourceWithAssetSearchResult: (action: string, newPath: string, contextChangedFromURL: boolean, symbolTriggeringSwap: string) => void;
    revertAssetSwapsForIsDirtyCheck: () => unknown;
    deleteSelectedSymbolSwaps: () => void;
    generateAssetRootPath: (assetsOnDisplay) => unknown;
  }
}
