declare namespace vision {
  interface InjectDisplayValidationService {
    validationError: string | null;
    refreshDisplayContent: typeof ng.noop;
    restartURLParams: typeof ng.noop;
    start: () => void;
    stop: () => void;
  }
}
