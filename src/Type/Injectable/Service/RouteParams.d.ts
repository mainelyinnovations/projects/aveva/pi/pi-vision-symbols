declare namespace vision {
  interface InjectRouteParams {
    getDisplayId: () => number;
    getParamValue: (paramName: string) => string | undefined;
    hasTruthyParamValue: (paramName: string) => boolean;
    readonly isAdHoc: boolean;
    readonly isEditor: boolean;
    readonly isEventComparison: boolean;
    readonly isEventDetails: boolean;
    readonly isKiosk: boolean;
    readonly isNewDisplay: boolean;
    readonly isSidebarHidden: boolean;
  }
}
