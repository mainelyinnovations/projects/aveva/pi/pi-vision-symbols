declare namespace vision {
  interface InjectDisplaySymbolLayoutService {
    calculateAdjustedDeltaForGrid: (
      deltaX: number,
      deltaY: number,
      startLeftRelativeToDisplay: number,
      startTopRelativeToDisplay: number,
    ) => DisplaySymbolLayoutServiceDelta;
    discreteMoveSelectedSymbols: (deltaX: number, deltaY: number) => void;
    dragMoveSelectedSymbols: (deltaX: number, deltaY: number, allowSnapToGrid: boolean) => DisplaySymbolLayoutServiceDelta;
    getClickTargetCoordinatesRelativeToDisplay: (
      event: DisplaySymbolLayoutServiceEvent,
      rotation?: number,
    ) => DisplaySymbolLayoutServiceRelativeToDisplay;
    getDisplayAreaLayoutCoordinates: () => DisplaySymbolLayoutServiceLayoutCoordinates | undefined;
    nearestGridPoint: (coordinate: number) => number;
    roundToNearestIncrement: (num: number, increment: number) => number;
    scrollDisplayArea: (direction: string, delta: number, initialCoordinateInScrollableArea: number, adjustForSymbolEdgeOffScreen: boolean) => void;
  }

  interface DisplaySymbolLayoutServiceDelta {
    adjustedDeltaX: number;
    adjustedDeltaY: number;
  }

  interface DisplaySymbolLayoutServiceRelativeToDisplay {
    clickTargetCenterRelativeToDisplayX: number;
    clickTargetCenterRelativeToDisplayY: number;
  }

  interface DisplaySymbolLayoutServiceEvent {
    pageX: number;
    pageY: number;
  }

  interface DisplaySymbolLayoutServiceLayoutCoordinates {
    left: number;
    top: number;
    height: number;
    width: number;
    leftWithoutScroll: number;
    topWithoutScroll: number;
  }
}
