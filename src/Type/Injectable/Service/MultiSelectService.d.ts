declare namespace vision {
  interface InjectMultiSelectService {
    rubberband: MultiSelectServiceRubberband;
    clearRubberband: () => void;
    selectBoundedSymbols: (always?: boolean) => boolean | void;
    startRubberbandSelect: (startLocation: LocationCoordinatesXY) => void;
    updateRubberbandSelect: (currLoc: LocationCoordinatesXY) => void;
  }

  interface MultiSelectServiceRubberband {
    height: number;
    maxHeight: number;
    maxWidth: number;
    width: number;
    x: number;
    y: number;
  }
}
