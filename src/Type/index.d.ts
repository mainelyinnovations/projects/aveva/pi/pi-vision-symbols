/// <reference path="./Common/Index.d.ts" />
/// <reference path="./Data/Index.d.ts" />
/// <reference path="./Enum/Index.d.ts" />
/// <reference path="./Extensibility/Index.d.ts" />
/// <reference path="./Injectable/Index.d.ts" />

declare global {
  const angular: ng.IAngularStatic;

  interface Window {
    APPNAME: string;
    PIVisualization: vision.Visualization;
  }
}

export as namespace vision;
export = vision;
