declare namespace vision {
  interface DataShapeGauge {
    Indicator: number;
    SymbolName: string;
    Time: Date;
    Value: DataUpdateSingleValue;
    ValueScaleLabels: Array<string>;
    ValueScalePositions: Array<number>;
    Path?: string;
    Label?: string;
  }
}
