declare namespace vision {
  interface DataShapeTrend {
    StartTime: Date;
    EndTime: Date;
    IsUpdating: boolean;
    SymbolName: string;
    Traces: Array<DataShapeTrendTrace>;
    ValueScaleLabels: Array<string>;
    ValueScaleLimits: Array<number>;
    ValueScalePositions: Array<number>;
  }

  interface DataShapeTrendTrace {
    Value: DataUpdateSingleValue;
    LineSegments: Array<string>;
    ErrorPoints?: string;
    Label?: string;
    Path?: string;
    Units?: string;
  }
}
