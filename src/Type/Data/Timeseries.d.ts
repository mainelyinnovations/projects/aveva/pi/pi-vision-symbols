declare namespace vision {
  interface DataShapeTimeseries {
    Data: Array<DataShapeTimeseriesRow>;
    SymbolName: string;
  }

  interface DataShapeTimeseriesRow {
    StartTime: Date;
    EndTime: Date;
    Minimum: string;
    Maximum: string;
    Values: Array<PIValue>;
    DisplayDigits?: number;
    Units?: string;
    Path?: string;
    Label?: string;
  }
}
