declare namespace vision {
  interface DataShapeTable {
    Rows: Array<DataShapeTableRow>;
    SymbolName: string;
  }

  interface DataShapeTableRow {
    Path?: string;
    Label?: string;
    Time: Date;
    Value: PIValue;
    Units?: string;
    UniqueId?: string;
  }
}
