declare namespace vision {
  interface PIValue {
    $$hashKey: string;
    Value: string | number | Date;
    Time: Date;
  }
}
