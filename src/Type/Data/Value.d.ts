declare namespace vision {
  interface DataShapeValue {
    Path?: string;
    Label?: string;
    SymbolName: string;
    Time: Date;
    Value: DataUpdateSingleValue;
  }

  type DataUpdateSingleValue = number | string | Date;
}
