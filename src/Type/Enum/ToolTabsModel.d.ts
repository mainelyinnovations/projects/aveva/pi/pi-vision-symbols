declare namespace vision {
  enum ToolTabIndexes {
    Search = 0,
    Calculations = 1,
    Graphics = 2,
    Events = 3,
  }

  interface ExtensibilityToolTabIndexes {
    Search: ToolTabIndexes.Search;
    Calculations: ToolTabIndexes.Calculations;
    Graphics: ToolTabIndexes.Graphics;
    Events: ToolTabIndexes.Events;
  }
}
