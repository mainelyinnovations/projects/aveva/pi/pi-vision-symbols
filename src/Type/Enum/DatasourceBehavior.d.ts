declare namespace vision {
  enum DatasourceBehavior {
    None = 0,
    Single = 1,
    Multiple = 2,
  }

  interface ExtensibilityDatasourceBehavior {
    None: DatasourceBehavior.None;
    Single: DatasourceBehavior.Single;
    Multiple: DatasourceBehavior.Multiple;
  }
}
