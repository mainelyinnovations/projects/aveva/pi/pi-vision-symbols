declare namespace vision {
  enum InjectPIToastMode {
    SUCCESS = 0,
    NEUTRAL = 1,
    ERROR = 2,
    WARNING = 3,
  }
}
