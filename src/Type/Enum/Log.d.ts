declare namespace vision {
  enum LogSeverity {
    Error = 0,
    Warning = 1,
    Information = 2,
    Log = 3,
    Debug = 4,
  }

  enum LogClearType {
    DataUpdate = 0,
    EventSearch = 1,
    Search = 2,
    NewDisplay = 3,
    Manual = 4,
  }

  interface ExtensibilityLogSeverity {
    Error: LogSeverity.Error;
    Warning: LogSeverity.Warning;
    Information: LogSeverity.Information;
    Log: LogSeverity.Log;
    Debug: LogSeverity.Debug;
  }

  interface ExtensibilityLogClearType {
    DataUpdate: LogClearType.DataUpdate;
    EventSearch: LogClearType.EventSearch;
    Search: LogClearType.Search;
    NewDisplay: LogClearType.NewDisplay;
    Manual: LogClearType.Manual;
  }
}
