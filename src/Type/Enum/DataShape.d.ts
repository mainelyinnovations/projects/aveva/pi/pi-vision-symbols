declare namespace vision {
  enum DataShape {
    Value = "Value",
    Gauge = "Gauge",
    Trend = "Trend",
    Table = "Table",
    TimeSeries = "TimeSeries",
  }

  interface ExtensibilityDataShape {
    Value: DataShape.Value;
    Gauge: DataShape.Gauge;
    Trend: DataShape.Trend;
    Table: DataShape.Table;
    TimeSeries: DataShape.TimeSeries;
  }
}
