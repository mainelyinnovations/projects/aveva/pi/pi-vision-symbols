declare namespace vision {
  interface VisionIdentity {
    IsUserIdentity: boolean;
    Identifier: string;
    Name: string;
  }
}
