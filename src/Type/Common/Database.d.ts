declare namespace vision {
  interface DatabaseCollection {
    children: Array<DatabaseInfo>;
    parents: Array<DatabaseInfo>;
    parent: unknown;
  }

  interface DatabaseInfo {
    $$hashKey: string;
    CanNav: boolean;
    IsDatabase: boolean;
    IsDisabled: boolean;
    IsSearch: boolean;
    Name: string;
    Path: string;
    Title: string;
    Type: string;
  }
}
