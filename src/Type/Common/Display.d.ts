declare namespace vision {
  interface DisplayBase {
    Id: number;
    Name: string;
    RequestId: string;
    Revision: number;
    DisplayProperties: DisplayBaseProperties;
  }

  interface DisplayBaseProperties {
    EventFrameSearch: DisplayPropertiesEventFrameModel;
    DataSources: Array<string>;
    PinnedEvents: Array<EventComparisonPinnedEvent>;
  }

  interface DisplayInfo {
    Id: number;
    Name: string;
    Path: string;
    Owner: string;
    StorageType: number;
    HasLabels: false;
    Thumbnail: string;
    DisplayLink: string;
    IsDisplayOwner: boolean;
  }

  interface Display {
    Id: number;
    Name: string;
    Path: string;
    RequestId: string;
    Revision: number;
    ReadOnly: boolean;
    HasWriteAccess: boolean;
    LegacyDisplay: boolean;
    EventFramePath?: string;
    DisplayProperties?: DisplayProperties;
    Symbols?: Array<SymbolVis<BaseVisionScope<BaseSymbolConfig>>>;
  }

  interface DisplayState {
    Id: number;
    Name: string;
    RequestId: string;
    Revision: number;
    EventFramePath: string | null;
    DisplayProperties: DisplayProperties;
    Symbols: BaseSymbolList;
    savePending: boolean;
  }

  interface DisplayProperties {
    BackgroundColor: string;
    showAssetPaths: boolean;
    Calculations: Array<DisplayCalculation>;
    GridSize?: number;
    FitAll?: number;
    context?: DisplayPropertiesContext;
    EventFrameSearch?: DisplayPropertiesEventFrameModel;
  }

  interface DisplayPropertiesContext {
    action?: string;
    searchCriteria?: AssetSearchCriteria;
  }

  interface DisplayDefaults {
    BackgroundColor: string;
  }

  interface DisplayDefaultsCustom {
    Value: object;
    Revision: number;
  }

  interface TimeRange {
    startTime: string;
    endTime: string;
  }
}
