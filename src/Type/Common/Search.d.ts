declare namespace vision {
  interface AssetSearchCriteria {
    AFServerDatabaseName: string;
    RootName: string;
    MaxResultsNumber: number;
  }

  interface AssetSearchFilter {
    Acknowledged: string;
    AFServerDatabaseName: string | null;
    AllDescendants: boolean;
    Annotated: string;
    Ascending: boolean;
    CategoryName: string | null;
    AttributeValues: Array<AttributeValueSearch>;
    EFTemplateName: string | null;
    EFTemplateId: string | null;
    ElementNames: string;
    ElementTemplateName: string | null;
    EventName: string | null;
    EventState: string;
    MaxDurationInSec: number | null;
    MinDurationInSec: number | null;
    SearchMode: string;
    Severity: string | null;
  }

  interface SearchFilter {
    Name: string;
    Operator: string;
    Value: string | number | Date;
  }

  interface AttributeValueSearch extends SearchFilter {
    TemplateId: string;
  }
}
