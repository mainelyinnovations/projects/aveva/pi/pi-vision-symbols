declare namespace vision {
  interface DisplayAttachment {
    Id: string;
    Name: string;
    Data?: string;
    isNew: boolean;
  }
}
