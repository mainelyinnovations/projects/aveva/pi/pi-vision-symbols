declare namespace vision {
  interface Datasource {
    Name: string;
    Path: string;
    Title: string;
    Type: string;
    IsAsset: boolean;
    IsDisabled: boolean;
    IsPlottable: boolean;
  }

  interface DatasourceMetadata {
    Path: string;
    DataType: string;
    Description: string;
    Minimum: number;
    Maximum: number;
    HasTarget: boolean;
  }
}
