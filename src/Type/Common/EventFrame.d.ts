declare namespace vision {
  interface EventFrame {
    Id: string;
    EventName: string;
    TemplateName: string;
    TemplateID: string;
    Path: string;
    PrimaryPath?: string;
    RefElemPath?: string;
    StartTime?: string;
    StartTimeISO?: string;
    StartTimeFormatted: boolean;
    EndTime?: string;
    EndTimeISO?: string;
  }

  interface EventFrameSearchFilter {
    AFServerDatabaseName?: string;
    EventName?: string;
    EFTemplateName?: string;
    EFTemplateId?: string;
    ElementNames?: Array<string>;
    SearchMode?: string;
    startTime?: string;
    endTime?: string;
    ElementTemplateName?: string;
    CategoryName?: string;
    Ascending?: boolean;
    maxCount?: number;
    MinDurationInSec?: number;
    MaxDurationInSec?: number;
    isAssetsOnDisplay?: boolean;
    AllDescendants?: boolean;
    Annotated: string;
    Acknowledged: string;
    EventState: string;
    Severity?: string;
    AttributeValues?: Array<EventFrameSearchFilterAttribute>;
  }

  interface EventFrameSearchFilterAttribute {
    Name: string;
    TemplateId: string;
    Value: string;
    Operator: string;
  }

  interface DisplayPropertiesEventFrameModel {
    Database: string | null;
    StartTime: string | null;
    EndTime: string | null;
    SearchMode: string;
    NumberOfResults: number;
    TimeRangeOptions: number;
    SearchQueryInstance: DisplayPropertiesEventFrameSearch;
    RefreshEventList: boolean;
    HasBeenConfigured?: boolean;
    AssetOnDisplay?: boolean;
    DisplayContext?: boolean;
  }

  interface DisplayPropertiesEventFrameSearch {
    AllDescendants: boolean;
    CategoryName?: string | null;
    ElementName: string | null;
    Name: string | null;
    ElementReferenceTemplate?: string | null;
    InProgress?: boolean | null;
    IsAcknowledged?: boolean | null;
    IsAnnotated?: boolean | null;
    MaxDuration: number | null;
    MinDuration: number | null;
    Severity?: string | null;
    Template: string | null;
    TemplateID: string | null;
    sortOrder?: boolean | null;
  }

  // Doesn't look like this is used with OverlayTrendQuery for RootCauseList
  interface EventFrameState {
    path: string;
    rootCauseDuration: number;
    shown: boolean;
    palIndex: number;
    markerIndex: number;
    isPinned: boolean;
    hasError: boolean;
  }
}
