declare namespace vision {
  interface DisplayCalculation {
    Name: string;
    Description: string | null;
    AssetContext: string;
    Server: string;
    Expression: string | null;
    IntervalMode: string;
    UOM: string;
    CalcInterval: string;
    SyncTime: string;
    ConversionFactor: string;
    Stepped: boolean;
  }
}
