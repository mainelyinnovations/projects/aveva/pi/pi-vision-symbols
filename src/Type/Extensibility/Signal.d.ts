declare namespace vision {
  interface SignalHelper {
    subscribers: Array<unknown>;
    subscribe: (callback: typeof ng.noop) => void;
    unsubscribe: (callback: typeof ng.noop) => void;
    raise: (param1: unknown, param2: unknown, param3: unknown, param4: unknown) => void;
  }
}
