declare namespace vision {
  interface SymbolDefinition<C, S, I extends keyof SymbolDefinitionInjectMap = never> {
    typeName: string;
    displayName?: string;
    datasourceBehavior?: DatasourceBehavior;
    iconUrl?: string;
    templateUrl?: string;
    configTemplateUrl?: string;
    configTitle?: string;
    configure?: any;
    StateVariables?: [string] | [string, string | undefined];
    StateProperties?: { [key: string]: string };
    inject?: ReadonlyArray<I>;
    resizerMode?: ResizerMode;
    noExpandSelector?: string;
    supportsCollections?: boolean;
    supportsDynamicSearchCriteria?: boolean;
    visObjectType: SymbolVis<S, I>;
    getDefaultConfig?: () => C;
    loadConfig?: (config: C) => boolean;
    configOptions?: () => Array<string | ConfigOptionsBase>;
    configInit?: (scope: BaseVisionScope<C>) => void;
    selectSymbolDefaultsFromConfig?: (config: C) => C;
    loadSymbol?: (scope: BaseVisionScope<C>, elem: JQuery) => void;
  }

  type SymbolDefinitionArgs<T extends keyof SymbolDefinitionInjectMap> = T extends keyof SymbolDefinitionInjectMap
    ? [SymbolDefinitionInjectMap[T]]
    : never;

  interface SymbolVis<S, I extends keyof SymbolDefinitionInjectMap = never> {
    scope: S;
    elem: JQuery;
    prototype: {
      init(scope: BaseVisionScope<BaseSymbolConfig>, elem: JQuery, ...args: SymbolDefinitionArgs<I>): void;
    };
    onDataUpdate?: <D>(data: D) => void;
    onConfigChange?: <C extends BaseSymbolConfig>(newConfig?: C, oldConfig?: C) => void;
    onResize?: (width: number, height: number) => void;
    onMultistateUpdate?: () => void;
  }

  interface SymbolVisCollection<S, I extends keyof SymbolDefinitionInjectMap = never> extends SymbolVis<S, I> {
    CollectionParams: {
      AFServerDatabaseName: string;
      RootName: string;
      RootElementID: string;
    };
  }

  type InjectedServices<T extends readonly (keyof SymbolDefinitionInjectMap)[]> = {
    [K in keyof T]: SymbolDefinitionInjectMap[T[K]];
  };

  // Helper type to map service names to their interface types
  type ServiceNameToInterface<T extends keyof SymbolDefinitionInjectMap> = {
    [K in keyof SymbolDefinitionInjectMap]: K extends T ? SymbolDefinitionInjectMap[K] : never;
  }[keyof SymbolDefinitionInjectMap];

  interface SymbolDefinitionInjectMap {
    // Angular Services
    $anchorScroll: ng.IAnchorScrollService;
    $cacheFactory: ng.ICacheFactoryService;
    $compile: ng.ICompileService;
    $controller: ng.IControllerService;
    $document: ng.IDocumentService;
    $exceptionHandler: ng.IExceptionHandlerService;
    $filter: ng.IFilterService;
    $http: ng.IHttpService;
    $httpBackend: ng.IHttpBackendService;
    $httpParamSerializer: ng.IHttpParamSerializer;
    $httpParamSerializerJQLike: ng.IHttpParamSerializer;
    $interpolate: ng.IInterpolateService;
    $interval: ng.IIntervalService;
    $locale: ng.ILocaleService;
    $location: ng.ILocationService;
    $log: ng.ILogService;
    $parse: ng.IParseService;
    $q: ng.IQService;
    $rootElement: ng.IRootElementService;
    $rootScope: ng.IRootScopeService;
    $sanitize: (s: string) => string;
    $sce: ng.ISCEService;
    $sceDelegate: ng.ISCEDelegateService;
    $templateCache: ng.ITemplateCacheService;
    $templateRequest: ng.ITemplateRequestService;
    $timeout: ng.ITimeoutService;
    $window: ng.IWindowService;

    // Vision Services
    adHocService: InjectAdHocService;
    appClipboard: InjectAppClipboard;
    appData: InjectAppData;
    assetContext: InjectAssetContext;
    assetSwapService: InjectAssetSwapService;
    configLinkService: InjectConfigLinkService;
    //CONSTANTS: InjectAppData; // exposed from PV
    dataPump: InjectDataPump;
    diffGenerator: InjectDiffGenerator;
    displayEventSearchService: InjectDisplayEventSearchService;
    displayProvider: InjectDisplayProvider;
    displaySymbolLayoutService: InjectDisplaySymbolLayoutService;
    displayValidationService: InjectDisplayValidationService;
    eventCache: InjectEventCache;
    gridSettings: InjectGridSettings;
    labelUpdateService: InjectLabelUpdateService;
    log: InjectLog; // This logs to console and should likely not be used in production.
    multiselectService: InjectMultiSelectService;
    paneSplitters: InjectPaneSplitters;
    PiToast: InjectPIToast;
    reloadDisplayService: InjectReloadDisplayService;
    routeParams: InjectRouteParams;
    //symbolCatalog: InjectMultiSelectService; // exposed from PV
    timeProvider: InjectTimeProvider;
    timeZoneSettings: InjectTimeZoneSettings;
    toolTabsModel: InjectToolTabsModel;
    webServices: InjectWebServices;
  }

  type ResizerMode = "" | "AutoWidth" | "RetainAspectRatio";
  type ConfigOptionsBase = {
    title?: string;
    mode?: string;
    enabled?: boolean;
    action?: (context: any) => void;
  };

  interface SymbolMultiState {
    DefaultStateValues: [boolean | null];
    ErrorStateValues: [string, boolean];
    IsDigitalSet: boolean;
    IsDynamicLimit: boolean;
    LowerValue: number;
    StateProperty: string;
    StateVariables: Array<string>;
    States: Array<SymbolMultiStateCase>;
  }

  interface SymbolMultiStateCase {
    StateValues: [string, boolean];
    UpperValue: number;
  }
}
