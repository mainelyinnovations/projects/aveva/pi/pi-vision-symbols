declare namespace vision {
  interface Visualization {
    AppData: unknown;
    AssetNameOption: unknown;
    AssetNameOptionLocalized: unknown;
    BarChartConfig: unknown;
    BaseSymbolHost: unknown;
    readonly CONSTANTS: VisualizationConstants;
    CalculationChangePending: unknown;
    Calculations: unknown;
    ClientSettings: unknown;
    ContextMenu: unknown;
    ContextMenuOptions: unknown;
    ContextPath: string;
    CurrentCulture: string;
    CurrentUICulture: string;
    DateTimeFormatter: unknown;
    DateTimeFormatterSettings: unknown;
    DisplayDiffTypes: unknown;
    EFRuntime: unknown;
    EventComparisonDisplay: unknown;
    EventSeverities: unknown;
    EventUpdateRateOption: unknown;
    Extensibility: VisualizationExtensibility;
    ExtensionCatalog: unknown;
    FontMetrics: unknown;
    GanttBar: unknown;
    GanttBarCollection: unknown;
    GanttChildrenStates: unknown;
    GaugeConfig: unknown;
    GraphicConfig: unknown;
    GraphicFill: unknown;
    IdentityContext: unknown;
    LabelUpdateService: VisualizationUpdateService;
    LegendButtonStates: unknown;
    LineConfig: unknown;
    Markers: unknown;
    MobileSiteRedirection: unknown;
    RadialGauge: unknown;
    ResourceStrings: VisionResourceStrings;
    SVGTrendCursor: unknown;
    SVGTrendModel: unknown;
    Scales: unknown;
    Search: unknown;
    SecurityContext: unknown;
    Signal: unknown;
    SymbolBase: unknown;
    SymbolRuntime: unknown;
    TableDataModel: unknown;
    TableMoveColumnHandler: unknown;
    TableResizeColumnHandler: unknown;
    TableRowDragHandler: unknown;
    TextResize: {
      adjustIntegerFont: <C>(position: BaseSymbolRuntimeDataPosition<C>, forceChange: boolean) => void;
      calculateFontSize: <C>(rt: BaseSymbolRuntimeData<C>, config: C, lineCount: number) => number;
      getTextWidth: () => number;
      heightToPoint: <C>(rt: BaseSymbolRuntimeData<C>, config: C) => number;
      setPositionFromFont: <C>(rt: BaseSymbolRuntimeData<C>, config: C) => void;
      shownLines: <C>(rt: BaseSymbolRuntimeData<C>) => number;
      descenderAdjustment: number;
      pixelsToPointsConversion: number;
    };
    TimeZoneSettings: unknown;
    TimebarModel: unknown;
    TrendConfig: unknown;
    TrendEnums: unknown;
    TrendLegendResizer: unknown;
    TrendUtils: unknown;
    Utils: VisualizationUtils;
    XYPlotConfig: unknown;
    deriveVisualizationFromBase: (symbolVis: SymbolVis<any, any>) => void;
    displayDefaults: unknown;
    displayDefaultsFromServer: unknown;
    editorData: unknown;
    eventColors: unknown;
    eventCompData: unknown;
    getDerivedTableDefinitionFromTemplate: unknown;
    isPublisher: boolean;
    labelSettingsInit: <C = BaseSymbolConfig, R = BaseSymbolRuntimeData<C>>(scope: BaseVisionScope<C, R>) => void;
    multiStateColors: unknown;
    navLinkAllowPattern: unknown;
    navigationLinkAllowPattern: unknown;
    navigationLinkSecurityOverride: unknown;
    parseDisplayDefaults: unknown;
    passiveToggleSupported: boolean;
    stringFormat: unknown;
    symbolCatalog: SymbolCatalog;
    timeBarDurations: unknown;
    timeZoneSettings: unknown;
    toolCatalog: unknown;
    version: string;
    xyAutoRange: unknown;
    xyFormat: unknown;
    xyPairData: unknown;
  }

  interface SymbolCatalog {
    register: <C, S, I extends keyof SymbolDefinitionInjectMap = never>(symbolDefinition: SymbolDefinition<C, S, I>) => void;
    defs: unknown;
  }

  interface VisualizationExtensibility {
    Enums: VisualizationExtensibilityEnums;
  }

  interface VisualizationExtensibilityEnums {
    DatasourceBehaviors: ExtensibilityDatasourceBehavior;
  }

  interface VisualizationUtils {
    parsePath: (path: string) => {
      label: string;
    };
    isSafeColor: (color: string) => boolean;
  }

  interface VisualizationConstants {
    AF_SERVER_PREFIX: string;
    PI_SERVER_PREFIX: string;
    CALC_PREFIX: string;
    AFCALC_PREFIX: string;
    AF_SERVER_FULLPREFIX: string;
    PI_SERVER_FULLPREFIX: string;
    AF_SERVER_PREFIX_LEN: number;
    PI_SERVER_PREFIX_LEN: number;
    CALC_PREFIX_LEN: number;
    AFCALC_PREFIX_LEN: number;

    // Separators
    ASSET_SEP: string;
    ATTRIBUTE_SEP: string;
    SERVER_SEP: string;
    AFCALC_SEP: string;

    // Data types
    ARCHIVE: string;
    ASSET: string;
    AFCALC: string;
    ATTRIBUTE: string;
    ATTRIBUTE_CATEGORY: string;
    DATABASE: string;
    TAG: string;

    // Counts
    MARKER_COUNT: number;
    PALETTE_COUNT: number;
    PALETTE_COUNT_PINNED: number;

    // Z-Indexes
    OVERLAY_LAYER: number;
    CONTEXT_MENU_LAYER: number;
    PANEL_LAYER: number;
    SPLITBAR_LAYER: number;
    MODAL_LAYER: number;
  }
}
