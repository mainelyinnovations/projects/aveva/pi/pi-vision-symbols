declare namespace vision {
  interface BaseVisionScope<C, R = BaseSymbolRuntimeData<C>> {
    $$childHead: unknown;
    $$childTail: unknown;
    $$destroyed: boolean;
    $$isolateBindings: unknown;
    $$listenerCount: unknown;
    $$listeners: unknown;
    $$nextSibling: unknown;
    $$phase: unknown;
    $$prevSibling: unknown;
    $$suspended: boolean;
    $$watchers: unknown;
    $$watchersCount: number;
    $id: number;
    $parent: unknown;
    $root: unknown;
    activateMoveAndEndHandlers: unknown;
    config: C;
    deactivateMoveAndEndHandlers: unknown;
    def: unknown;
    hostElem: JQuery;
    isMultiple: boolean;
    isNewShape: boolean;
    isShape: boolean;
    onTemplateLoaded: unknown;
    position: unknown;
    resizers: unknown;
    runtimeData: R;
    symTemplate: string;
    symbol: SymbolScope<C>;
    tooltip: string;
    collectionEditClickShieldActive: boolean;
    dropOverCollectionDisabled: boolean;
    isCollectionEdit: boolean;
    kioskMode: boolean;
    layoutMode: boolean;
    touchEnabled: boolean;
  }

  type BaseSymbolList = Array<SymbolScopeBase>;
  type SymbolScopeBase = SymbolScope<BaseSymbolConfig>;

  interface SymbolScope<C> {
    Configuration: C;
    DataSources: Array<string>;
    MSDataSources: Array<string>;
    Name: string;
    SymbolType: string;
  }

  interface SymbolScopeHasStartEndTime {
    StartTime: string;
    EndTime: string;
  }

  interface SymbolScopeHasLabel {
    Label: string;
  }

  interface BaseSymbolRuntimeData<C> {
    name: string;
    position: BaseSymbolRuntimeDataPosition<C>;
    isSelected: boolean;
    selectedUsingTouch: boolean;
    metaData: BaseSymbolRuntimeDataMetaData;
    data: unknown; // TODO: Data changes based on types and the proper union needs to be built.
    def: SymbolDefinition<C, any>;
    labelSettings?: (settings: LabelSettings) => LabelSettingsReturn;
  }

  interface BaseSymbolRuntimeDataMetaData {
    DataType: string;
    HasTarget: boolean;
    Minimum: number;
    Maximum: number;
    Path: string;
    datasources: Array<string>;
    lastUpdatedTime: number;
    supportsUOMConversion: boolean;
  }

  interface BaseSymbolRuntimeDataPosition<C> {
    _c: number;
    _h: number;
    _l: number;
    _r: number;
    _rt: number;
    _t: number;
    _w: number;
    _config: C;
    _syncConfig: boolean;
    syncConfig: boolean;
    bottom: number;
    center: number;
    height: number;
    left: number;
    middle: number;
    right: number;
    rotation: number;
    rotationInRadians: number;
    top: number;
    width: number;
    boundingBoxCoordinates: {
      bottom: number;
      left: number;
      right: number;
      top: number;
    };
    cornerPoints: {
      bottomLeft: LocationCoordinatesXY;
      bottomRight: LocationCoordinatesXY;
      topLeft: LocationCoordinatesXY;
      topRight: LocationCoordinatesXY;
    };
    setNewPositionCallback: () => void;
  }

  interface LabelSettings extends LabelSettingsReturn {
    description?: string;
  }

  interface LabelSettingsReturn {
    nameType?: string;
    customName?: string;
  }

  interface LocationCoordinatesXY {
    x: number;
    y: number;
  }
}
